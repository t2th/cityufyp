
#include <ArduinoJson.h>
#include "LedControl.h"
#include <Servo.h>
#include <Wire.h>        //I2C Arduino Library
#include <VL53L0X.h>
//#include <HMC5883L.h>
#define addr 0x1E         //I2C Address for The HMC5883

#define LONG_RANGE

#define HIGH_SPEED
//#define HIGH_ACCURACY

const bool IS_DEBUG = false;
const String MODEL_NAME = "car01";

const byte servo_bot_pin = A0;
const byte servo_top_pin = 11;
const byte servo_hand_pin = 3;
const byte inPin_leftB = 6;
const byte inPin_leftA = 9;
const byte inPin_rightA = 10;
const byte inPin_rightB = 5;

const byte left_pin = 7;
const byte right_pin = 8;
//const byte bot_left_pin = 12;//4;
//const byte bot_right_pin = 13;//2;

const byte o_distance_pin = A1;

//HMC5883L compass;
//MagnetometerScaled valueOffset;

VL53L0X lSensor;
Servo servoBot, servoTop, servoHand;
String inData;
StaticJsonBuffer<200> jsonBuffer;
LedControl lc = LedControl(A2, 2, A3, 1);

String lastMovementCmd = " ";

byte pos1 = 0, pos2 = 0, handpos = 0;    //currentPos
byte apos1 = 10, apos2 = 10; //addressPos
byte tpos1 = 90 + apos1, tpos2 = 90 + apos2, thandpos = 0; //targetPos
byte spos1 = 60 + apos1, spos2 = 30 + apos2; //smallest Pos
byte mpos1 = 120 + apos1, mpos2 = 140 + apos2; //max Pos
byte mhandpos = 40;

byte rotationCounter = 0;
byte rotationSkip = 5;
int eyeCounter = 0;
byte reportCounter = 0;
int o_distance = 0;
int l_distance = 0;
int x = 0, y = 0, z = 0;
float headingDegrees;

void setup() {
  pinMode(inPin_leftA, OUTPUT);
  pinMode(inPin_leftB, OUTPUT);
  pinMode(inPin_rightA, OUTPUT);
  pinMode(inPin_rightB, OUTPUT);
  pinMode(left_pin, INPUT);
  pinMode(right_pin, INPUT);
  //pinMode(bot_left_pin, INPUT);
  //pinMode(bot_right_pin, INPUT);
  pinMode(o_distance_pin, INPUT);

  lc.shutdown(0, false);
  lc.setIntensity(0, 5);
  lc.clearDisplay(0);
  showOnLed( new byte[8][8] {
    {1, 1, 1, 1, 1, 1, 1, 1},
    {1, 1, 1, 1, 1, 1, 1, 1},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {1, 1, 1, 1, 1, 1, 1, 1},
    {1, 1, 1, 1, 1, 1, 1, 1}
  });
  servoBot.attach(servo_bot_pin);
  servoTop.attach(servo_top_pin);
  servoHand.attach(servo_hand_pin);
  //delay(800);
  int lcRow = 0;
  int lcCounter = 0;
  for (pos1 = 0; pos1 <= 180; pos1 += 1) { // goes from 0 degrees to 180 degrees
    if (pos1 < mpos1 && pos1 > spos1) 
      servoBot.write(pos1);              // tell servo to go to position in variable 'pos'
    if (pos1 < mpos2 && pos1 > spos2) {
      pos2 = pos1;
      servoTop.write(pos2);
    }
    if (handpos < mhandpos) {
      handpos = pos1;
      servoHand.write(handpos);
    }
    lcCounter++;
    if (lcCounter > 3) {
      lcCounter = 0;
      lc.clearDisplay(0);
      for (int i = 0; i < 8; i++) {
        lc.setLed(0, lcRow, i, 1);
      }
      lcRow++;
      if (lcRow > 8)
        lcRow = 0;
    }
    delay(50);                       // waits 15ms for the servo to reach the position
  }
  // xyz rotation
  Wire.begin();
  Wire.beginTransmission(addr); //start talking
  Wire.write(0x02);             // Set the Register
  Wire.write(0x00);             // Tell the HMC5883 to Continuously Measure
  Wire.endTransmission();
  //se
  lSensor.init();
  lSensor.setTimeout(100);
#if defined LONG_RANGE
  // lower the return signal rate limit (default is 0.25 MCPS)
  lSensor.setSignalRateLimit(0.1);
  // increase laser pulse periods (defaults are 14 and 10 PCLKs)
  lSensor.setVcselPulsePeriod(VL53L0X::VcselPeriodPreRange, 18);
  lSensor.setVcselPulsePeriod(VL53L0X::VcselPeriodFinalRange, 14);
#endif

#if defined HIGH_SPEED
  // reduce timing budget to 20 ms (default is about 33 ms)
  lSensor.setMeasurementTimingBudget(20000);
#elif defined HIGH_ACCURACY
  // increase timing budget to 200 ms
  lSensor.setMeasurementTimingBudget(200000);
#endif
  Serial.begin(9600);
  showOnLed( new byte[8][8] {
    {0, 0, 1, 0, 0, 1, 0, 0},
    {0, 0, 1, 0, 0, 1, 0, 0},
    {0, 0, 1, 0, 0, 1, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 1},
    {0, 1, 0, 0, 0, 0, 1, 0},
    {0, 0, 1, 1, 1, 1, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0}
  });

  /* haha a ok emoji
    showOnLed( new byte[8][8] {
      {0,0,0,0,1,0,0,0},
      {0,0,0,0,1,1,0,0},
      {0,0,0,0,0,1,0,0},
      {0,0,0,0,0,1,1,0},
      {0,0,0,1,1,1,1,1},
      {0,0,1,0,0,0,1,1},
      {0,0,0,1,0,0,1,0},
      {0,0,0,0,1,1,0,0}
    });*/

}

void loop() {
  while (Serial.available() > 0)  {
    char recieved = Serial.read();
    if (recieved != '\n')
      inData += recieved;

    // Process message when new line character is recieved
    if (recieved == '\n')    {
      if (IS_DEBUG) {
        Serial.print("Arduino Received: ");
        Serial.println(inData);
      }
      String command = getValue(inData, ' ', 0);
      if (command == "m")
        reportModel();
      if (command == "w") {
        goUp(160);
        lastMovementCmd = "w";
      }
      if (command == "s") {
        goBack(100);
        lastMovementCmd = "s";
      }
      if (command == "a") {
        goLeft(100);
        lastMovementCmd = "a";
      }
      if (command == "d") {
        goRight(100);
        lastMovementCmd = "d";
      }
      if (command == " ") {
        goStop();
        lastMovementCmd = " ";
      }
      if (command == "c") {
        int angle = max(min(getValue(inData, ' ', 1).toInt(), mhandpos), 0);
        //servoBot.write(angle);
        thandpos = angle;
      }
      if (command == "q") {
        int angle = max(min(getValue(inData, ' ', 1).toInt() + apos1, mpos1), spos1);
        if (getValue(inData, ' ', 1) == "-1")
          angle = pos1;
        tpos1 = angle;
      }
      if (command == "e") {
        int angle = max(min(getValue(inData, ' ', 1).toInt() + apos2, mpos2), spos2);
        if (getValue(inData, ' ', 1) == "-1")
          angle = pos2;
        tpos2 = angle;
      }
      if (command == "f") {
        String face = getValue(inData, ' ', 1);
        if (face.length() >= 64)
          for (int i = 0; i < 64; i++) {
            if (face[i] == '1')
              lc.setLed(0, i % 8, i / 8, 1);
            else
              lc.setLed(0, i % 8, i / 8, 0);
          }
      }
      if (command == "b")
        checkIfBlock();
      inData = ""; // Clear recieved buffer
    }
  }

  //servo
  rotationCounter++;
  if (rotationCounter > rotationSkip) {
    if (thandpos > mhandpos)
      thandpos = mhandpos;
    if (handpos < thandpos) {
      servoHand.write(handpos++);
    } else if (handpos > thandpos) {
      servoHand.write(handpos--);
    }
    if (pos1 < tpos1) {
      servoBot.write(pos1++);
    } else if (pos1 > tpos1) {
      servoBot.write(pos1--);
    }
    if (pos2 < tpos2) {
      servoTop.write(pos2++);
    } else if (pos2 > tpos2) {
      servoTop.write(pos2--);
    }
    rotationCounter = 0;
  }


  reportCounter ++;
  if (reportCounter > 5) {
    o_distance = analogRead(o_distance_pin);
    //xyz rotation
    updateCompass();

    reportStatus();
    reportCounter = 0;
    l_distance = lSensor.readRangeSingleMillimeters();
    delay(30);
  } else {
    delay(50);
  }


}
void reportStatus() {
  JsonObject& root = jsonBuffer.createObject();
  root["_m"] = MODEL_NAME;
  root["s1"] = pos1 - apos1;
  root["s2"] = pos2 - apos2;
  root["sh"] = handpos;
  root["l"] = digitalRead(left_pin);
  root["r"] = digitalRead(right_pin);
  //root["bl"] = digitalRead(bot_left_pin);
  //root["br"] = digitalRead(bot_right_pin);
  root["a"] = lastMovementCmd;
  root["deg"] = headingDegrees;
  root["od"] = o_distance;
  root["ld"] = l_distance;
  String tmp = "";
  root.printTo(tmp);
  Serial.println(tmp);
  jsonBuffer.clear();
}
void reportModel() {
  Serial.println("car-1");
}

void lookLeft() {
  servoBot.write(45);
}
void lookRight() {
  servoBot.write(135);
}

void goUp(int up_speed) {
  analogWrite(inPin_leftA, 0);
  analogWrite(inPin_rightA, 0);
  analogWrite(inPin_leftB, up_speed);
  analogWrite(inPin_rightB, up_speed);
}
void goLeft(int left_speed) {
  analogWrite(inPin_leftA, left_speed);
  analogWrite(inPin_rightB, left_speed);
  analogWrite(inPin_leftB, 255);
  analogWrite(inPin_rightA, 255);
}
void goRight(int right_speed) {
  analogWrite(inPin_leftB, right_speed);
  analogWrite(inPin_rightA, right_speed);
  analogWrite(inPin_leftA, 255);
  analogWrite(inPin_rightB, 255);
}
void goBack(int back_speed) {
  analogWrite(inPin_leftB, back_speed);
  analogWrite(inPin_rightB, back_speed);
  analogWrite(inPin_leftA, 255);
  analogWrite(inPin_rightA, 255);
}
void goStop() {
  analogWrite(inPin_leftA, 0);
  analogWrite(inPin_leftB, 0);
  analogWrite(inPin_rightA, 0);
  analogWrite(inPin_rightB, 0);
}
void checkIfBlock() {
  if (IS_DEBUG) {
    Serial.println("check if block");
    Serial.println(digitalRead(left_pin)); //1 = not blocked
    Serial.println(digitalRead(right_pin));
    //Serial.println(digitalRead(bot_left_pin));
    //Serial.println(digitalRead(bot_right_pin));
  }
}

String getValue(String data, char separator, int index) {
  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length() - 1;

  for (int i = 0; i <= maxIndex && found <= index; i++) {
    if (data.charAt(i) == separator || i == maxIndex) {
      found++;
      strIndex[0] = strIndex[1] + 1;
      strIndex[1] = (i == maxIndex) ? i + 1 : i;
    }
  }

  return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

void showOnLed(byte input[][8]) {
  for (int i = 0; i < 8; i++) {
    for (int j = 0; j < 8; j++) {
      if (input[i][j] == 1)
        lc.setLed(0, j, i, 1);
      else
        lc.setLed(0, j, i, 0);
    }
  }
}

void updateCompass() {
  return;
  /*
  // Retrive the raw values from the compass (not scaled).
  MagnetometerRaw raw = compass.readRawAxis();
  // Retrived the scaled values from the compass (scaled to the configured scale).
  MagnetometerScaled scaled = compass.readScaledAxis();

  scaled.XAxis -= valueOffset.XAxis;
  scaled.YAxis -= valueOffset.YAxis;
  scaled.ZAxis -= valueOffset.ZAxis;

  // Values are accessed like so:
  int MilliGauss_OnThe_XAxis = scaled.XAxis;// (or YAxis, or ZAxis)

  // Calculate heading when the magnetometer is level, then correct for signs of axis.
  float yxHeading = atan2(scaled.YAxis, scaled.XAxis);
  float zxHeading = atan2(scaled.ZAxis, scaled.XAxis);

  float heading = yxHeading;

  // Once you have your heading, you must then add your 'Declination Angle', which is the 'Error' of the magnetic field in your location.
  // Find yours here: http://www.magnetic-declination.com/
  // -2 37' which is -2.617 Degrees, or (which we need) -0.0456752665 radians, I will use -0.0457
  float declinationAngle = -0.0503;
  heading += declinationAngle;

  // Correct for when signs are reversed.
  if (heading < 0)
    heading += 2 * PI;

  // Check for wrap due to addition of declination.
  if (heading > 2 * PI)
    heading -= 2 * PI;

  // Convert radians to degrees for readability.
  headingDegrees = heading * 180 / M_PI;
  */
}

