// Pin 12:Data in, Pin 11: Clock,  Pin 10: CS(Load)
#include "LedControl.h"
LedControl lc = LedControl(A2, 2,A3, 1);

void setup()
{
  Serial.begin(9600);
  Serial.println("shutdownng");
  lc.shutdown(0,false);  // 關閉省電模式
  Serial.println("setIntensity");
  lc.setIntensity(0,15);  // 設定亮度為 8 (介於0~15之間)
  Serial.println("clearDis");
  lc.clearDisplay(0);    // 清除螢幕
}
void loop()
{
      int randNumber_col = random(8);
      int randNumber_row = random(8);
      lc.setLed(0,randNumber_col,randNumber_row,1); // 將Led的欄,列設定為亮
      delay(100);
      lc.setLed(0,randNumber_col,randNumber_row,0); // 將Led的欄,列設定為暗
}
