import collections
import numpy as np
import cv2
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile
from pathlib import Path

from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image

from object_detection.utils import label_map_util as label_map_util

from object_detection.utils import visualization_utils as vis_util

# This is needed since the notebook is stored in the object_detection folder.
class Detector(object):
  def __init__(self):
    self.started = False
  def start(self):
    #tf.device('/cpu:0')
    print("Model preparation", file=sys.stdout)
    # What model to download.
    #https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/detection_model_zoo.md

    #MODEL_NAME = 'D:/CityU/cityufyp/Server/Server_main/ssdlite_mobilenet_v2_coco_2018_05_09'
    MODEL_NAME = 'D:/CityU/cityufyp/Server/Server_main/ssd_mobilenet_v2_coco_2018_03_29'
    MODEL_FILE = MODEL_NAME + '.tar.gz'
    DOWNLOAD_BASE = 'http://download.tensorflow.org/models/object_detection/'

    # Path to frozen detection graph. This is the actual model that is used for the object detection.
    PATH_TO_CKPT = MODEL_NAME + '/frozen_inference_graph.pb'

    # List of the strings that is used to add correct label for each box.
    PATH_TO_LABELS = 'D:/CityU/cityufyp/Server/Server_main/object_detection/data/mscoco_label_map.pbtxt'

    NUM_CLASSES = 90

    print("Checking Model", file=sys.stdout)
    my_file = Path(MODEL_FILE)
    if not my_file.is_file():
      print("Download Model", file=sys.stdout)
      MODEL_NAME ='ssd_mobilenet_v2_coco_2018_03_29'
      MODEL_FILE = MODEL_NAME + '.tar.gz'
      opener = urllib.request.URLopener()
      opener.retrieve(DOWNLOAD_BASE + MODEL_FILE, MODEL_FILE)
    tar_file = tarfile.open(MODEL_FILE)
    for file in tar_file.getmembers():
      file_name = os.path.basename(file.name)
      if 'frozen_inference_graph.pb' in file_name:
        tar_file.extract(file, os.getcwd())
        detection_graph = tf.Graph()

    print("Load a (frozen) Tensorflow model into memory.", file=sys.stdout)
    with detection_graph.as_default():
      od_graph_def = tf.GraphDef()
      with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

    label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
    categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
    self.category_index = label_map_util.create_category_index(categories)

    config = tf.ConfigProto()
    #config=tf.ConfigProto(intra_op_parallelism_threads=4)
    #config.gpu_options.allow_growth = True
    #session = tf.Session(config=config)
    #config = tf.ConfigProto()
    #config.gpu_options.per_process_gpu_memory_fraction = 0.7
    #session = tf.Session(config=config)
    
    #config.log_device_placement = True
    #sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))

    detection_graph.as_default()
    self.sess = tf.Session(config=config,graph=detection_graph)
    #with detection_graph.as_default():
    # Definite input and output Tensors for detection_graph
    self.image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
    # Each box represents a part of the image where a particular object was detected.
    self.detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
    # Each score represent how level of confidence for each of the objects.
    # Score is shown on the result image, together with the class label.
    self.detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
    self.detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
    self.num_detections = detection_graph.get_tensor_by_name('num_detections:0')
    self.started = True

  def load_image_into_numpy_array(self,image):
    (im_width, im_height) = image.size
    return np.array(image.getdata()).reshape(
        (im_height, im_width, 3)).astype(np.uint8)
  def resultToArray(self,
                                                boxes,
                                                classes,
                                                scores,
                                                category_index,
                                                instance_masks=None,
                                                keypoints=None,
                                                use_normalized_coordinates=False,
                                                max_boxes_to_draw=20,
                                                min_score_thresh=.5,
                                                agnostic_mode=False,
                                                line_thickness=4):
    # Create a display string (and color) for every box location, group any boxes
    # that correspond to the same location.
    
    box_to_display_str_map = []
    box_to_color_map = collections.defaultdict(str)
    box_to_instance_masks_map = {}
    box_to_keypoints_map = collections.defaultdict(list)
    if not max_boxes_to_draw:
      max_boxes_to_draw = boxes.shape[0]
    for i in range(min(max_boxes_to_draw, boxes.shape[0])):
      if scores is None or scores[i] > min_score_thresh:
        jsonObject = {}
        jsonObject["box"] = tuple(boxes[i].tolist())
        box = str(tuple(boxes[i].tolist()))
        if instance_masks is not None:
          box_to_instance_masks_map[box] = instance_masks[i]
        if keypoints is not None:
          box_to_keypoints_map[box].extend(keypoints[i])
        if scores is None:
          box_to_color_map[box] = 'black'
        else:
          if not agnostic_mode:
            if classes[i] in category_index.keys():
              class_name = category_index[classes[i]]['name']
            else:
              class_name = 'N/A'
            display_str = '{}: {}%'.format(
                class_name,
                int(100*scores[i]))
          else:
            display_str = 'score: {}%'.format(int(100 * scores[i]))
          jsonObject["name"] = class_name
          jsonObject["score"] = int(100*scores[i])
          box_to_display_str_map.append(jsonObject)
          #box_to_display_str_map[box].append(display_str)
          #if agnostic_mode:
          #  box_to_color_map[box] = 'DarkOrange'
          #else:
          #  box_to_color_map[box] = STANDARD_COLORS[
          #      classes[i] % len(STANDARD_COLORS)]
          
    return dict(results=box_to_display_str_map)


  def check(self, img):
    #detection_graph = self.detection_graph
     # Capture frame-by-frame
    #ret, frame = cap.read()
    #frame = img
    # Our operations on the frame come here
    #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    #latestImage = frame
    
          # the array based representation of the image will be used later in order to prepare the
    # result image with boxes and labels on it.
    image_np = self.load_image_into_numpy_array( Image.fromarray(img))
    # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
    
    image_np_expanded = np.expand_dims(image_np, axis=0)
    # Actual detection.
    (boxes, scores, classes, num) = self.sess.run(
      [self.detection_boxes, self.detection_scores, self.detection_classes, self.num_detections],
      feed_dict={self.image_tensor: image_np_expanded})
    #vis_util.visualize_boxes_and_labels_on_image_array(
    #  image_np,
    #  np.squeeze(boxes),
    #  np.squeeze(classes).astype(np.int32),
    #  np.squeeze(scores),
    #  category_index,
    #  use_normalized_coordinates=True,
    #  line_thickness=8)
    return self.resultToArray(
        np.squeeze(boxes),
        np.squeeze(classes).astype(np.int32),
        np.squeeze(scores),
        self.category_index,
        use_normalized_coordinates=True,
        line_thickness=8)

