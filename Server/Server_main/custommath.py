import math
import numpy as np
#point = [y, x]
#rect = [y1, x1, y2, x2]
def isPointInRect(point, rect):
    if(rect[0]<=point[0] and point[0]<=rect[2] and
       rect[1]<=point[1] and point[1]<=rect[3]):
       return True
    return False
#point = [y, x]
def distanceBetween(p1, p2):
    return np.power(((p2[0]-p1[0])**2 + (p2[1]-p1[1])**2), 1/2)

    
#point = [y, x]
#rect = [y1, x1, y2, x2]
def getCenterPoint(rect):
    return [(rect[2]+rect[0]) /2, (rect[3]+rect[1]) /2]

def boxToSize(box):
    return [box[2]-box[0], box[3]-box[1]]

def distanceBetween3D(p1, p2):
    return np.sqrt((p2[0]-p1[0])**2 +(p2[1]-p1[1])**2 +(p2[2]-p1[2])**2 )