import sys
import os
import cv2
from cv2 import aruco
import numpy as np
import math
import numpy.linalg as la
import json
import requests
import time
import scipy.linalg as sla
#import mathutils
#from math import radians
#from scipy.linalg import expm
def isRotationMatrix(R) :
    Rt = np.transpose(R)
    shouldBeIdentity = np.dot(Rt, R)
    I = np.identity(3, dtype = R.dtype)
    n = np.linalg.norm(I - shouldBeIdentity)
    return n < 1e-6
def rotationMatrixToEulerAngles(R) :
 
    assert(isRotationMatrix(R))
     
    sy = math.sqrt(R[0,0] * R[0,0] +  R[1,0] * R[1,0])
     
    singular = sy < 1e-6
 
    if  not singular :
        x = math.atan2(R[2,1] , R[2,2])
        y = math.atan2(-R[2,0], sy)
        z = math.atan2(R[1,0], R[0,0])
    else :
        x = math.atan2(-R[1,2], R[1,1])
        y = math.atan2(-R[2,0], sy)
        z = 0
 
    return np.array([x, y, z])
    
def rotation_matrix(axis, theta):
    """
    Return the rotation matrix associated with counterclockwise rotation about
    the given axis by theta radians.
    """
    axis = np.asarray(axis)
    axis = axis/math.sqrt(np.dot(axis, axis))
    a = math.cos(theta/2.0)
    b, c, d = -axis*math.sin(theta/2.0)
    aa, bb, cc, dd = a*a, b*b, c*c, d*d
    bc, ad, ac, ab, bd, cd = b*c, a*d, a*c, a*b, b*d, c*d
    return np.array([[aa+bb-cc-dd, 2*(bc+ad), 2*(bd-ac)],
                     [2*(bc-ad), aa+cc-bb-dd, 2*(cd+ab)],
                     [2*(bd+ac), 2*(cd-ab), aa+dd-bb-cc]])
def rot_euler(v, xyz):
    ''' Rotate vector v (or array of vectors) by the euler angles xyz '''
    #https://stackoverflow.com/questions/6802577/rotation-of-3d-vector
    for theta, axis in zip(xyz, np.eye(3)):
        v = np.dot(np.array(v), sla.expm(np.cross(np.eye(3), axis*-theta)))
    return v
    
def getRTfromMatrix44 ( M) :
    u,s,vh = np.linalg.svd(M, full_matrices=True)
    rpure = u * vh
    R = cv2.Rodrigues(rpure)
    T = np.array(([M[0, 3], M[1,3], M[2,3]]))
    print(R)
    print(T)
    return R, T
def cyl2cat(r, theta, z):
    return (r*np.cos(theta), r*np.sin(theta), z)
def applyEuler(vector, euler):
    pass
    #vec = mathutils.Vector(vector[0], vector[1], vector[2]))
    #eul = mathutils.Euler(euler[0], euler[1], euler[2], 'XYZ').to_matrix()
    #return vec * eul
camera_id = 'sup01'
last_update = time.time()
update_cooldown = 0 #second

cap = cv2.VideoCapture(0)
with np.load('camera.npz') as X:
    mtx, dist, _, _ = [X[i] for i in ('mtx','dist','rvecs','tvecs')]
            
    aruco_dict = aruco.Dictionary_get( aruco.DICT_6X6_1000 )
    markerLength = 10 # Here, our measurement unit is centimetre.	
    arucoParams = aruco.DetectorParameters_create()
    ret, img = cap.read()
    h,  w = img.shape[:2]
    newcameramtx, roi=cv2.getOptimalNewCameraMatrix(mtx,dist,(w,h),1,(w,h))
    mapx,mapy = cv2.initUndistortRectifyMap(mtx,dist,None,newcameramtx,(w,h),5)
    while(True):
        try:
            ret, img = cap.read()
            #img = cv2.imread(imgFileNames[i], cv2.IMREAD_COLOR)
            imgRemapped = cv2.remap(img, mapx, mapy, cv2.INTER_LINEAR, cv2.BORDER_CONSTANT) # for fisheye remapping
            imgRemapped_gray = cv2.cvtColor(imgRemapped, cv2.COLOR_BGR2GRAY)    # aruco.etectMarkers() requires gray image
            corners, ids, rejectedImgPoints = aruco.detectMarkers(imgRemapped_gray, aruco_dict, parameters=arucoParams) # Detect aruco
            print(ids)
            if ids != None: # if aruco marker detected
                rvecs, tvecs, _ = aruco.estimatePoseSingleMarkers(corners, markerLength, mtx, dist) # For a single marker
                imgWithAruco = aruco.drawDetectedMarkers(imgRemapped, corners, ids, (0,255,0))
                imgWithAruco = aruco.drawAxis(imgWithAruco, mtx, dist, rvecs, tvecs, 10)    # axis length 100 can be changed according to your requirement
                
                if 2 in ids:
                    #getRTfromMatrix44
                    idx = list(ids).index(2)
                    try:
                        _r  = cv2.Rodrigues(rvecs[idx])[0]
                        _r2 = rotationMatrixToEulerAngles(_r)
                        pos = rot_euler(tvecs[idx][0], [_r2[0], _r2[1], 0])
                        pos[0] = -pos[0]
                        #_r2 = np.transpose(_r)
                        #print("2")
                        #_r3 = np.negative(_r2)* tvecs[0]
                        #pos = pos[0]
                        print(pos)
                        
                        print('break')
                        current = time.time()
                        elapsed = current - last_update
                        if(elapsed>update_cooldown):
                            des = tvecs[idx][0]
                            #for i in range(3):
                            #    des[i] = des[i]
                            s = json.dumps({'did':camera_id,'pos':pos.tolist(), 'tvec':des.tolist(), 'rvec':_r2.tolist()})
                            try:
                                res = requests.post("http://127.0.0.1:6801/update_ar_pos", json=s, timeout=0.1).json()
                                #print(res['status'])
                                if res['status']==0:
                                    last_update = current
                            except:
                                pass
            #            print(tvecs[0][0])
            #            print(tvecs[0][0][0]) #x
            #            print(tvecs[0][0][2]) #y
            #            print(tvecs[0][0][2]) #z
                    except Exception as e:
                        print(e)
            else:   # if aruco marker is NOT detected
                imgWithAruco = imgRemapped  # assign imRemapped_color to imgWithAruco directly
            cv2.imshow("aruco", imgWithAruco)   # display
            if cv2.waitKey(2) & 0xFF == ord('q'):   # if 'q' is pressed, quit.
                break
        except Exception as e:
            print(e)
            pass