import numpy as np
import cv2
from flask import Flask, render_template, Response, request, redirect, url_for,  jsonify
from object_detection.detector import Detector
from werkzeug.utils import secure_filename
import sys
import os
import json
from collections import namedtuple
import threading
from flask_socketio import SocketIO, emit
from scipy.linalg import expm
from datetime import datetime
from vision import ObjectIdentifier, ObjectLocator, CameraSetting, VisionCore
import traceback
import base64
import time
from cv2 import aruco
import math
import numpy.linalg as la
import scipy.linalg as sla
import traceback
class DeviceManager(object):
    posDict = {}
    deviceDict = {}
    def __init__(self,detector, did,socketio, sid):
        self.detector = detector
        self.did = did
        self.image = None
        self.objectDetectionLoopThread = None
        self.arPositioningLoopThread = None
        self.socketio = socketio
        self.lastData = {}
        self.lastUpdateTime = datetime.now()
        self.lastInSightObject = []
        self.ip = "0.0.0.0"
        self.arid = -1
        self.position = [0, 0, 0]
        self.arrotation = [0, 0, 0]
        self.lastRvec = None
        self.nparr = None
        self.objectDetected = True
        self.arPositioned = True
        self.sid = sid

    def feed(self, data):
        try:
            if("image" in data.keys()):
                self.image = base64.b64decode( data["image"])
                self.nparr = np.fromstring(self.image, np.uint8)
                self.lastUpdateTime = datetime.now()
                self.objectDetected = False
                self.arPositioned = False
            if("skipImage" in data.keys()):
                self.lastUpdateTime = datetime.now()
                self.objectDetected = False
                self.arPositioned = False
            data.pop("image", None)
            self.arid = data["arid"]
            self.lastData = data
        except:
            traceback.print_exc()

    def feedArMarker(self, data):
        for marker in data:
            marker["tvec"] = np.array(marker["tvec"])
            marker["rvec"] = np.array(marker["rvec"])
        result = self.arPositioningLoopThread.processMarkers(data)
        self.socketio.emit("update_ar_position", result, namespace='/robot', broadcast = True, json=True)
        result = {'status':0, 'data': DeviceManager.posDict}
        self.socketio.emit('ar_pos_update', result, namespace='/positioning', broadcast=True)
    
    def feedObjects(self, data):
        self.objectDetectionLoopThread.feedObjectDetectionResult(data)
    def on_objects_update(self):
        jsonObj = [(x.__dict__) for x in VisionCore.getInstance().objectList]
        result = {'status':0, 'data': jsonObj} 
        self.socketio.emit('objects_update', result, namespace='/positioning', broadcast=True)
        self.socketio.emit('objects_update', result, namespace='/robot', broadcast=True)
    def start(self):
        self.objectDetectionLoopThread = ObjectDetectionLoopThread(self)
        self.objectDetectionLoopThread.start()
        self.arPositioningLoopThread = ArPositioningLoopThread(self)
        self.arPositioningLoopThread.start()
        time.sleep(1)

    def stop(self):
        self.objectDetectionLoopThread.stop()
        self.arPositioningLoopThread.stop()
        
class ArPositioningLoopThread(threading.Thread):
    staticMarker =dict(
        [
            (2, {"pos":[0,0,0], "rotate": [0,0,0]})
        ]
    )
    def __init__(self,owner):
        threading.Thread.__init__(self, daemon=False, args=())
        self.isStop = False
        self.owner =owner
        self.cameraMatrix = np.load('c270.npz')
        self.mtx, self.dist, _, _ = [self.cameraMatrix[i] for i in ('mtx','dist','rvecs','tvecs')] 
        self.aruco_dict = aruco.Dictionary_get( aruco.DICT_6X6_1000 )
        self.markerLength = 10 # Here, our measurement unit is centimetre.	
        self.arucoParams = aruco.DetectorParameters_create()
        w = 640
        h = 480
        newcameramtx, roi=cv2.getOptimalNewCameraMatrix(self.mtx,self.dist,(w,h),1,(w,h))
        self.mapx,self.mapy = cv2.initUndistortRectifyMap(self.mtx,self.dist,None,newcameramtx,(w,h),5)

    def stop(self):
        self.isStop = True    
    
    def analysis(self):
        image = cv2.imdecode(self.owner.nparr, cv2.IMREAD_COLOR)
        imgRemapped = cv2.remap(image, self.mapx, self.mapy, cv2.INTER_LINEAR, cv2.BORDER_CONSTANT) # for fisheye remapping
        imgRemapped_gray = cv2.cvtColor(imgRemapped, cv2.COLOR_BGR2GRAY)    # aruco.etectMarkers() requires gray image
        corners, ids, rejectedImgPoints = aruco.detectMarkers(imgRemapped_gray, self.aruco_dict, parameters=self.arucoParams) # Detect aruco
        result = {'did':self.owner.did}
        if ids is not None: # if aruco marker detected
            rvecs, tvecs, _ = aruco.estimatePoseSingleMarkers(corners, self.markerLength, self.mtx, self.dist) # For a single marker
            #imgWithAruco = aruco.drawDetectedMarkers(imgRemapped, corners, ids, (0,255,0))
            #imgWithAruco = aruco.drawAxis(imgWithAruco, self.mtx, self.dist, rvecs, tvecs, 10)    # axis length 100 can be changed according to your requirement
            listMarker = []
            listId = []
            for o in ids: listId.append(int(o[0]))
            for i in range(len(listId)): 
                listMarker.append({
                    'id': int(listId[i]),
                    'tvec': tvecs[i][0],
                    'rvec': rvecs[i]
                    })
            result = self.processMarkers(listMarker)
            print(listMarker)
        #result['markers'] = [] 
        self.owner.socketio.emit("update_ar_position", result, namespace='/robot', broadcast = True, json=True)

    def processMarkers(self, listMarker):
        result = {'did':self.owner.did}
        staticMarker = [x for x in listMarker if x["id"]==2]
        if len(staticMarker) >0:
            #processing location
            marker = staticMarker[0]
            try:
                _r  = cv2.Rodrigues(marker["rvec"])[0]
                _r2 = rotationMatrixToEulerAngles(_r)
                _r3 = np.transpose(_r)
                #_r4 = rotationMatrixToEulerAngles(_r3)
                pos = np.negative(np.matmul(_r3, marker["tvec"]))
                #pos = rot_euler(tvecs[idx][0], _r2)
                #pos[2] = -pos[2]
                pos = [x*10 for x in pos.tolist()]
                #print(pos)
                #des = marker["tvec"][0]
                #print(pos)
                #print(self.owner.position)
                #print(F"===>>>>>>> {_r2}")
                _r2[2] = normalizeRadian(_r2[2]+math.pi)
                pos2 = smoothingList(self.owner.position, pos, 0.65)
                result['pos'] = pos2 #self position
                #result['tvec'] = des.tolist()
                result['rvec'] = _r2.tolist() #self rotation
                #s = json.dumps(result)
                DeviceManager.posDict[self.owner.did] = result
                self.owner.position = list(pos2)
                self.owner.arrotation = _r2.tolist()
                self.owner.lastRvec = marker["rvec"]
                
            except Exception as e:
                traceback.print_exc()
                
        targetedId = set.intersection(set([x["id"] for x in listMarker]), set([x.arid for k, x in DeviceManager.deviceDict.items() if x.arid>0])) 
        targetedDevice = {x.arid: x.did for k,x in DeviceManager.deviceDict.items() if x.arid in targetedId}
        if(len(targetedId) >0):
            print ("scanned more correct ar marker")
            for marker in [x for x in listMarker if (x["id"] in targetedId)] :
                deviceId = targetedDevice[marker["id"]]
                print(deviceId)
                _r  = cv2.Rodrigues(marker['rvec'])[0]
                _dr  = cv2.Rodrigues(self.owner.lastRvec)[0]
                _dr2 = np.transpose(_dr)
                pos = marker['tvec']
                pos =(np.matmul(_dr2, pos))
                pos = [x*10 for x in pos.tolist()]
                pos = [self.owner.position[i]+pos[i] for i in range(3)]
                #rota = rotationMatrixToEulerAngles(np.matmul(_r,la.inv(_dr))).tolist()
                #rota[0] =normalizeRadian(rota[0]- math.pi/2)
                pitchDelta = 0
                # if("pitchDelta" in self.owner.lastData):
                #     pitchDelta = self.owner.lastData["pitchDelta"]
                rota = rotationMatrixToEulerAngles(
                    np.matmul(createRotationMatrix(0, 0, - math.pi/2 + pitchDelta)
                    , np.matmul(_r, la.inv(_dr)))).tolist()
                DeviceManager.deviceDict[deviceId].position = pos
                DeviceManager.deviceDict[deviceId].arrotation = rota
                temp = {'did':deviceId}
                temp['pos']= list(pos)
                temp['rvec']  = list(rota)
                DeviceManager.posDict[deviceId] = temp
        return result

    def run(self):
        print('ar loop thread of '+self.owner.did+' started!')
        while (not self.isStop):
            try:
                tdelta = datetime.now()- self.owner.lastUpdateTime
                if(tdelta.total_seconds()>0.5  or self.owner.arPositioned or self.owner.image is None):
                    time.sleep(0.035)
                    continue
                self.analysis()
                self.owner.arPositioned = True
                result = {'status':0, 'data': DeviceManager.posDict}
                self.owner.socketio.emit('ar_pos_update', result, namespace='/positioning', broadcast=True)
            except Exception as e:
                traceback.print_exc()
            finally:
                time.sleep(0.035)
        print('ar loop thread on '+self.owner.did+' stopped!')
def normalizeRadian(i):
    if(i<0):
        i = (-i)% (math.pi * 2)
        return (math.pi * 2)-i
    i = i% (math.pi * 2)
    return i
class ObjectDetectionLoopThread(threading.Thread):
    def __init__(self,owner):
        threading.Thread.__init__(self, daemon=False, args=())
        self.isStop = False
        self.owner =owner

    def stop(self):
        self.isStop = True    
    def feedObjectDetectionResult(self, checkResult):
        deviceType = "mobile"
        rotation =self.owner.arrotation
        location = self.owner.position
        cameraSetting = CameraSetting.getByName(deviceType)
        checkResult = ObjectIdentifier.identify(checkResult, location, rotation,cameraSetting)
        checkResult = ObjectLocator.locate(checkResult, location,rotation,cameraSetting)
        self.owner.lastInSightObject = checkResult
        print(self.owner.lastInSightObject)
        self.owner.on_objects_update()
        for obj in checkResult["results"]:
            if("oid" not in obj):
                obj["oid"] = ""
            if("pos" not in obj):
                obj["pos"] = [0,0,0]
        self.owner.socketio.emit("update_in_sight", checkResult, namespace='/robot', room=self.owner.sid)
        
    def analysis(self):
        s = self.owner.lastData
        deviceId = s['did']
        deviceType = s['type']
        distance = -1
        if('distance' in s.keys()):
            distance = s['distance']

        rotation =self.owner.arrotation
        # if('ry' in s.keys() and 'rx' in s.keys()):
        #     rotatey = s['ry']
        #     rotatez = s['rx']
        #     rotation =  [rotatey, rotatez]
        location = self.owner.position
        if(len(deviceId)>0):
            print("analysis call by {}".format(deviceId))
        print('going to analysis')
        #start = datetime.now()
        # convert string of image data to uint8
        cameraSetting = CameraSetting.getByName(deviceType)
        if(deviceType == "robot"):
            # decode image
            img = cv2.imdecode(self.owner.nparr, cv2.IMREAD_COLOR)
            img = cv2.resize(img, (300, 300))
            checkResult =  self.owner.detector.check(img)
            print(checkResult)
            checkResult = ObjectIdentifier.identify(checkResult, location, rotation,cameraSetting)
            checkResult = ObjectLocator.locate(checkResult, location,rotation,cameraSetting)
            if(distance>0):
                checkResult = ObjectLocator.locateCenter(checkResult, location,rotation,cameraSetting, distance)
        elif(deviceType=="mobile"):
            checkResult = []
        self.owner.lastInSightObject = checkResult
        print(self.owner.lastInSightObject)
        self.owner.on_objects_update()
        
        for obj in checkResult["results"]:
            if("oid" not in obj):
                obj["oid"] = ""
            if("pos" not in obj):
                obj["pos"] = [0,0,0]

        self.owner.socketio.emit("update_in_sight", checkResult, namespace='/robot', room=self.owner.sid, json=True)
        
    def run(self):
        print('object detection thread of '+self.owner.did+' started!')
        while (not self.isStop):
            try:
                tdelta = datetime.now()- self.owner.lastUpdateTime
                if(tdelta.total_seconds()>0.5 or self.owner.objectDetected or self.owner.image is None):
                    time.sleep(0.035)
                    continue
                self.analysis()
                self.owner.objectDetected = True
            except Exception as e:
                traceback.print_exc()
            finally:
                time.sleep(0.035)
        print('object detection thread on '+self.owner.did+' stopped!')



def isRotationMatrix(R) :
    Rt = np.transpose(R)
    shouldBeIdentity = np.dot(Rt, R)
    I = np.identity(3, dtype = R.dtype)
    n = np.linalg.norm(I - shouldBeIdentity)
    return n < 1e-6
def rotationMatrixToEulerAngles(R) :
 
    assert(isRotationMatrix(R))
     
    sy = math.sqrt(R[0,0] * R[0,0] +  R[1,0] * R[1,0])
     
    singular = sy < 1e-6
 
    if  not singular :
        x = math.atan2(R[2,1] , R[2,2])
        y = math.atan2(-R[2,0], sy)
        z = math.atan2(R[1,0], R[0,0])
    else :
        x = math.atan2(-R[1,2], R[1,1])
        y = math.atan2(-R[2,0], sy)
        z = 0
 
    return np.array([x, y, z])
    
def rotation_matrix(axis, theta):
    """
    Return the rotation matrix associated with counterclockwise rotation about
    the given axis by theta radians.
    """
    axis = np.asarray(axis)
    axis = axis/math.sqrt(np.dot(axis, axis))
    a = math.cos(theta/2.0)
    b, c, d = -axis*math.sin(theta/2.0)
    aa, bb, cc, dd = a*a, b*b, c*c, d*d
    bc, ad, ac, ab, bd, cd = b*c, a*d, a*c, a*b, b*d, c*d
    return np.array([[aa+bb-cc-dd, 2*(bc+ad), 2*(bd-ac)],
                     [2*(bc-ad), aa+cc-bb-dd, 2*(cd+ab)],
                     [2*(bd+ac), 2*(cd-ab), aa+dd-bb-cc]])
def rot_euler(v, xyz):
    ''' Rotate vector v (or array of vectors) by the euler angles xyz '''
    for theta, axis in zip(xyz, np.eye(3)):
        v = np.dot(np.array(v), sla.expm(np.cross(np.eye(3), axis*-theta)))
    return v
    
def getRTfromMatrix44 ( M) :
    u,s,vh = np.linalg.svd(M, full_matrices=True)
    rpure = u * vh
    R = cv2.Rodrigues(rpure)
    T = np.array(([M[0, 3], M[1,3], M[2,3]]))
    print(R)
    print(T)
    return R, T
def cyl2cat(r, theta, z):
    return (r*np.cos(theta), r*np.sin(theta), z)

def smoothingList(old, new, alpha):
    return [smoothing(old[i], new[i], alpha) for i in range(len(old))]

def smoothing(old, new, alpha):
    return old * (1-alpha) + new * alpha
def createRotationMatrix(yaw, pitch, roll):
    yawMatrix = np.matrix([
    [math.cos(yaw), -math.sin(yaw), 0],
    [math.sin(yaw), math.cos(yaw), 0],
    [0, 0, 1]
    ])

    pitchMatrix = np.matrix([
    [math.cos(pitch), 0, math.sin(pitch)],
    [0, 1, 0],
    [-math.sin(pitch), 0, math.cos(pitch)]
    ])

    rollMatrix = np.matrix([
    [1, 0, 0],
    [0, math.cos(roll), -math.sin(roll)],
    [0, math.sin(roll), math.cos(roll)]
    ])

    return yawMatrix * pitchMatrix * rollMatrix