from custommath import *
import math
import numpy as np
import uuid
import numpy.linalg as la
import scipy.linalg as sla

import threading
import time
import cv2
import traceback
import json


MAX_DIFFERENCE_PER_IDENTIFY = 3

class DetectedObject(object):
    minTrust = 0
    maxTrust = 120
    def __init__(self, name, pos, size):
        self.name = name
        self.pos = pos
        self.size = size
        self.trust = 3
        self.oid= str(uuid.uuid4())
    def reduceTrust(self):
        if(self.trust>DetectedObject.minTrust):
            self.trust -=1
        return self.trust
    def increaseTrust(self):
        if(self.trust<=DetectedObject.maxTrust):
            self.trust+=2
        return self.trust
    def feedPosition(self, newPos, alpha = 0.6):
        self.pos = smoothingList(self.pos, newPos, alpha)
    def feedSize(self, newSize, alpha = 0.35):
        self.size = smoothingList(self.size, newSize, alpha)
    def __str__(self):
        return json.dumps(self.__dict__)

class VisionCore(object):
    _instance = None
    def getInstance():
        if(VisionCore._instance is None):
            VisionCore._instance = VisionCore()
        return VisionCore._instance

    def __init__(self):
        self.loopThread = None
        self.objectList = []
    def getClosestObject(self, pos, minDistance = 99999):
        closestObj = None
        minD = minDistance
        for dObj in self.objectList:
            d = distanceBetween3D(pos, dObj.pos)
            print(d)
            if minD>d:
                closestObj = dObj
                minD = d
        return closestObj
    def addObject(self, obj):
        self.objectList.append(obj)
        
    def start(self):
        self.loopThread = LoopThread(self)
        self.loopThread.start()
        
    def stop(self):
        self.loopThread.stop()
class LoopThread(threading.Thread):
    def __init__(self,owner):
        threading.Thread.__init__(self, daemon=False, args=())
        self.isStop = False
        self.owner =owner

    def stop(self):
        self.isStop = True    
    def run(self):
        print('reducing trust started!')
        while (not self.isStop):
            try:
                self.owner.objectList = [x for x in self.owner.objectList if x.reduceTrust()>DetectedObject.minTrust ]
            except Exception as e:

                traceback.print_exc()
            finally:
                time.sleep(1)
        print('reducing trust stopped!')

class CameraSetting(object):
    def __init__(self, focal, sensorWidth, sensorHeight):
        self.focal = focal
        self.sensorWidth = sensorWidth
        self.sensorHeight = sensorHeight
        self.angleOfWidth = 2*np.arctan((sensorWidth/2) /focal)
        self.angleOfHeight = 2*np.arctan((sensorHeight/2) /focal)
        self.degreeOfWidth = self.angleOfWidth * (180/math.pi)
        self.degreeOfHeight = self.angleOfHeight* (180/math.pi)
    def getByName(name):
        return {
            "robot":CameraSetting(4, 3.19642857#3.58
            , 2.02),
            "mobile": CameraSetting(4.25, 5.6447997,4.2336)
        }.get(name, None)

class ObjectLocator(object):
    @staticmethod
    def locateCenter(objects, cameraPos, cameraDirection, cameraSetting, distance):
        if(distance<0):
            return objects

        centerObj = None
        minD = 100
        
        for dObj in objects["results"]:
            if(isPointInRect([0.5,0.5], dObj["box"])):
                d = distanceBetween([0.5, 0.5], getCenterPoint(dObj["box"]))
                if(minD>d):
                    centerObj = dObj
                    minD = d
        if(centerObj is not None):# and centerObj["name"] =="cell phone"):
            eSize = ObjectLocator.estimateSize(boxToSize(centerObj["box"]), cameraSetting, distance)
            ePos = ObjectLocator.estimateLocation(centerObj["box"], cameraPos, cameraDirection, cameraSetting, distance)
            print(F"\t\t\t\t{ePos}")
            if("oid" in centerObj):
                closestObj= next(x for x in VisionCore.getInstance().objectList if x.oid == centerObj["oid"])
                closestObj.feedPosition(ePos)
                closestObj.feedSize(eSize)
                closestObj.increaseTrust()
            else:
                closestObj = VisionCore.getInstance().getClosestObject(ePos, distance /5)
                if closestObj is not None and closestObj.name == centerObj["name"]:
                    print("existed object")
                    closestObj.feedPosition(ePos)
                    closestObj.feedSize(eSize)
                    closestObj.increaseTrust()
                else:
                    newObj = DetectedObject(centerObj["name"], ePos, eSize)
                    VisionCore.getInstance().addObject(newObj)
                    print("add new object")
        return objects

            
    @staticmethod
    def locate(objects, cameraPos, cameraDirection, cameraSetting):
        oList = VisionCore.getInstance().objectList
        print("\n\n BEFORE")
        print([str(x) for x in oList])
        for obj in objects["results"]:
            if("oid" not in obj):
                continue

            print("updaing exist object")
            detectedObject = next(x for x in oList if x.oid == obj["oid"])
            print(detectedObject)
            #objDistance = distanceBetween3D(cameraPos, detectedObject.pos)
            objDistance = ObjectLocator.estimateDistance(boxToSize(obj["box"]), cameraSetting, detectedObject.size)
            ePos = ObjectLocator.estimateLocation(obj["box"], cameraPos, cameraDirection, cameraSetting, objDistance)
            #eSize = ObjectLocator.estimateSize(boxToSize(obj["box"]), cameraSetting, objDistance)
            detectedObject.feedPosition(ePos)
            detectedObject.increaseTrust()
            #detectedObject.feedSize(eSize, 0.1)
        print("\n\n AFTER")
        print([str(x) for x in oList])
        return objects

    @staticmethod
    def estimateSize(objSize, cameraSetting, distance):
        result = [0, 0] #y(height), x(width)
        result[0] = distance * np.tan(cameraSetting.angleOfHeight * objSize[0] /2) * 2
        result[1] = distance * np.tan(cameraSetting.angleOfWidth * objSize[1] /2) * 2
        return result

    @staticmethod
    def estimateDistance(objSize, cameraSetting, objOriginSize):
        d0 = objOriginSize[0]/ (np.tan(cameraSetting.angleOfHeight * objSize[0] /2) * 2)
        d1 = objOriginSize[1]/ (np.tan(cameraSetting.angleOfWidth * objSize[1] /2) * 2)
        return (d0+d1 )/2

    @staticmethod
    def estimateLocation(objectBox, cameraPos, cameraDirection,cameraSetting, distance):
        
        cPoint = getCenterPoint(objectBox)
        #itemMat = createRotationMatrix(0,cameraSetting.angleOfWidth * (cPoint[1]-0.5),cameraSetting.angleOfHeight * (cPoint[0]-0.5) )
        itemMat = createRotationMatrix(0,
            np.arctan(np.tan(cameraSetting.angleOfWidth)* (cPoint[1]-0.5)),
            np.arctan(np.tan(cameraSetting.angleOfHeight)* -(cPoint[0]-0.5))
        )
        print(itemMat)
        cameraMat = createRotationMatrix(cameraDirection[2],cameraDirection[1],cameraDirection[0])
        print(cameraMat)
        realMat = np.matmul(itemMat, cameraMat)
        print(realMat)
        xyz = np.matmul(np.array([0,0,distance]), realMat).tolist()[0]
        print(F" from {xyz}")
        xyz = [cameraPos[i]+xyz[i] for i in range(3)]
        print(F" to xyz: {xyz}\n")
        return xyz

        # cPoint = getCenterPoint(objectBox)
        # theta = normalizeRadian( (cameraSetting.angleOfHeight * (cPoint[0]-0.5)))
        # phi = normalizeRadian( (cameraSetting.angleOfWidth * (cPoint[1]-0.5)))
        # #rota = [theta, phi, 0]
        # theta = normalizeRadian(cameraDirection[0] + (cameraSetting.angleOfHeight * (cPoint[0]-0.5)))
        # phi = normalizeRadian(cameraDirection[1]+ (cameraSetting.angleOfWidth * (cPoint[1]-0.5)))
        # rangeX = distance * np.tan(cameraSetting.angleOfWidth /  2)
        # rangeY = distance * np.tan(cameraSetting.angleOfWidth /  2)
        # x =(cPoint[1] * rangeX)-(rangeX/2)
        # x = -x
        # y =(cPoint[0] * rangeY)-(rangeX/2)
        # # x = cameraPos[0] +  distance * np.sin(theta) *  np.cos(phi) 
        # # y = cameraPos[1] +distance * np.sin(theta) * np.sin(phi)
        # # z = cameraPos[2] + distance * np.cos(theta)
        # print(F"\n from {[x,y,-distance]}")
        # xyz = rot_euler([x,y,-distance], [theta, phi, 0])
        # print(F" from {xyz}")
        # xyz = [cameraPos[i]+xyz[i] for i in range(3)]
        # print(F" to xyz: {xyz}\n")
        # return xyz

def normalizeRadian(i):
    if(i<0):
        i = (-i)% (math.pi * 2)
        return (math.pi * 2)-i
    i = i% (math.pi * 2)
    return i
def rot_euler(v, xyz):
    ''' Rotate vector v (or array of vectors) by the euler angles xyz '''
    #https://stackoverflow.com/questions/6802577/rotation-of-3d-vector
    for theta, axis in zip(xyz, np.eye(3)):
        v = np.dot(np.array(v), sla.expm(np.cross(np.eye(3), axis*-theta)))
    return v
    
def angleBetween(v1, v2):
    x = v1[0]-v2[0]
    y = v1[1]-v2[1]
    z = v1[2]-v2[2]
    yaw = math.atan2(x, z) *180.0/math.pi
    padj =  math.sqrt(x**2) + z**2
    pitch =  math.atan2(padj, y) *180.0/ math.pi
    return [yaw,pitch, 0]
class ObjectIdentifier(object):
    @staticmethod
    def estimateObjectInSight(cameraPos, cameraDirection, cameraSetting):
        objs = VisionCore.getInstance().objectList
        inSightResult = []
        #cmat = cv2.Rodrigues(np.array(cameraDirection).astype(float))[0]
        #ncmat = la.inv(cmat)
        cameraMat = createRotationMatrix(cameraDirection[2],cameraDirection[1],cameraDirection[0])
        inv_cmat = la.inv(cameraMat)
        #print(cmat.tolist())
        for obj in objs:
            #print(cameraPos)
            #print(obj.pos)
            # print("><><><><><>")
            # print(inv_cmat)
            # objMat =  cv2.Rodrigues(np.array(angleBetween(cameraPos, obj.pos)).astype(float))[0]
            # print(objMat)
            # diffMat = np.matmul(inv_cmat, objMat)
            # print(diffMat)
            # ncmat = diffMat
            # print(rotationMatrixToEulerAngles(ncmat))
            # print("><><><><><>")
            #objAngle = angleBetween(cameraPos, obj.pos)
            #omat = cv2.Rodrigues(np.array(objAngle).astype(float))[0]
            #nmat = la.inv(omat)
            objDistance = distanceBetween3D(cameraPos, obj.pos)
            xyz = [obj.pos[i] - cameraPos[i] for i in range(3)]
            #xyz = rot_euler(xyz,    rotationMatrixToEulerAngles(ncmat) )
            xyz = np.negative(np.matmul(inv_cmat, xyz)).tolist()[0]
            xyz[1] = -xyz[1]
            print(xyz)
            d = xyz[2]
            rangeX = d * np.tan(cameraSetting.angleOfWidth /  2)
            rangeY = d * np.tan(cameraSetting.angleOfHeight /  2)
            x = ((xyz[0] +rangeX)/ (rangeX*2))
            y =((xyz[1] +rangeY)/ (rangeY*2))
            eSize = ObjectIdentifier.estimateInScreenSize(obj.size, cameraSetting, objDistance)
            area =  eSize[0]*eSize[1]
            result = {"oid":obj.oid,"name": obj.name, "cx": x, "cy": y, "size": eSize, "area":area, "rSize": obj.size, "distance": objDistance}
            print(result)
            inSightResult.append(result)
        return inSightResult
            
    @staticmethod
    def estimateInScreenSize(eSize, cameraSetting, distance):
        result = [0, 0] #y(height), x(width)
        result[0] = (2 * np.arctan(eSize[0]/( 2*distance)))/cameraSetting.angleOfHeight
        result[1] = (2 * np.arctan(eSize[1]/ (2*distance)))/cameraSetting.angleOfWidth
        return result

    @staticmethod
    def getMatchingMark(screenArea, possibleObject):
            return math.fabs((screenArea-possibleObject["area"]) / possibleObject["area"])
    @staticmethod
    def identify(objects, cameraPos, cameraDirection, cameraSetting):
        #TODO: here
        try:
            temp = ObjectIdentifier.estimateObjectInSight(cameraPos, cameraDirection, cameraSetting)
            for obj in objects["results"]:
                b = obj["box"]
                y = [b[0],b[2]]
                x = [b[1],b[3]]
                listOfPossible = [o for o in temp if obj["name"]==o["name"] and x[0]-0.3 <= o["cx"] <= x[1]+0.3 and y[0]-0.3<=o["cy"] <= y[1]+0.3]
                screenArea = (y[1]-y[0] )*(x[1]-x[0])
                print(F"looking for {obj['name']} about {screenArea} at x: {x[0]} {x[1]} y: {y[0]} {y[1]}")
                if(listOfPossible is None or len(listOfPossible)<=0):
                    print("oh not there are no possible")
                else:
                    bestResult = min(listOfPossible, key=lambda x: ObjectIdentifier.getMatchingMark(screenArea, x))
                    print(F"matching: {bestResult} and marks = {ObjectIdentifier.getMatchingMark(screenArea, bestResult)}")
                    if(ObjectIdentifier.getMatchingMark(screenArea, bestResult) <= MAX_DIFFERENCE_PER_IDENTIFY):
                        print(F"matched: {bestResult['oid']}")
                        obj["oid"] = bestResult["oid"]
                        obj["size"] = bestResult["rSize"]
        except Exception as e:
            traceback.print_exc()
        return objects

def smoothingList(old, new, alpha):
    return [smoothing(old[i], new[i], alpha) for i in range(len(old))]

def smoothing(old, new, alpha):
    return old * (1-alpha) + new * alpha

def isRotationMatrix(R) :
    Rt = np.transpose(R)
    shouldBeIdentity = np.dot(Rt, R)
    I = np.identity(3, dtype = R.dtype)
    n = np.linalg.norm(I - shouldBeIdentity)
    return n < 1e-6
def rotationMatrixToEulerAngles(R) :
 
    assert(isRotationMatrix(R))
     
    sy = math.sqrt(R[0,0] * R[0,0] +  R[1,0] * R[1,0])
     
    singular = sy < 1e-6
 
    if  not singular :
        x = math.atan2(R[2,1] , R[2,2])
        y = math.atan2(-R[2,0], sy)
        z = math.atan2(R[1,0], R[0,0])
    else :
        x = math.atan2(-R[1,2], R[1,1])
        y = math.atan2(-R[2,0], sy)
        z = 0
 
    return np.array([x, y, z])

def createRotationMatrix(yaw, pitch, roll):
    yawMatrix = np.matrix([
    [math.cos(yaw), -math.sin(yaw), 0],
    [math.sin(yaw), math.cos(yaw), 0],
    [0, 0, 1]
    ])

    pitchMatrix = np.matrix([
    [math.cos(pitch), 0, math.sin(pitch)],
    [0, 1, 0],
    [-math.sin(pitch), 0, math.cos(pitch)]
    ])

    rollMatrix = np.matrix([
    [1, 0, 0],
    [0, math.cos(roll), -math.sin(roll)],
    [0, math.sin(roll), math.cos(roll)]
    ])

    return yawMatrix * pitchMatrix * rollMatrix