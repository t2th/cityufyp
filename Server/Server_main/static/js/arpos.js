var devicesThreeObject = {};
var objectsThreeObject = {};

var renderer, stats, scene, camera;
var loader = new THREE.FontLoader();
init();
animate();
/*
var THREE_Text = require('./templates/arpos.html');
var SpriteText2D = THREE_Text.SpriteText2D;
var textAlign = THREE_Text.textAlign;*/
function init() {
    container = document.createElement('div');
    document.body.appendChild(container);
    //
    scene = new THREE.Scene();
    scene.background = new THREE.Color(0xb0b0b0);
    //
    camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 1, 1000);
    camera.position.set(0, 0, 200);
    //
    var group = new THREE.Group();
    scene.add(group);
    //
    var directionalLight = new THREE.DirectionalLight(0xffffff, 0.6);
    directionalLight.position.set(0.75, 0.75, 1.0).normalize();
    scene.add(directionalLight);
    var ambientLight = new THREE.AmbientLight(0xcccccc, 0.2);
    scene.add(ambientLight);
    //
    //axes
    var axes = new THREE.AxesHelper(200);
    scene.add(axes);
    //grid xz
    var gridXY = new THREE.GridHelper(400, 20);
    gridXY.rotation.x = Math.PI / 2;
    scene.add(gridXY);
    //var helper = new THREE.GridHelper( 20, 1 );
    //helper.rotation.x = Math.PI / 2;
    //group.add( helper );
    //
    //var obj = initSVGObject();
    //addGeoObject( group, obj );
    //
    renderer = new THREE.WebGLRenderer({
        antialias: true
    });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    container.appendChild(renderer.domElement);
    //
    controls = new THREE.TrackballControls(camera);
    controls.rotateSpeed = 1.0;
    controls.zoomSpeed = 1.2;
    controls.panSpeed = 0.8;
    controls.noZoom = false;
    controls.noPan = false;
    controls.staticMoving = true;
    controls.dynamicDampingFactor = 0.3;
    //
    //stats = new Stats();
    //container.appendChild( stats.dom );
    //
    window.addEventListener('resize', onWindowResize, false);
    var material = new THREE.LineBasicMaterial({
        color: 0xffff00,
        linewidth: 10,
        linejoin: 'round'
    });
    var geometry = new THREE.Geometry();
    geometry.vertices.push(new THREE.Vector3(0, 0, 0));
    linex = new THREE.Line(geometry, material);
    liney = new THREE.Line(geometry, material);
    linez = new THREE.Line(geometry, material);
    material = new THREE.LineBasicMaterial({
        color: 0xff00ff,
        linewidth: 5
    });
    lined = new THREE.Line(geometry, material);
    scene.add(linex);
    scene.add(liney);
    scene.add(linez);
    scene.add(lined);
    /*
    textx =new SpriteText2D("x=10", { align: textAlign.center, font: '30px Arial', fillStyle: '#000000'})
    textx.position.set(0, -200, 0)
    textx.scale.set(1,1,1)
    scene.add(linex);*/
}
var o1, linex, liney, linez, lined;
//var textx,texty,textz,textd;
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}

function animate() {
    requestAnimationFrame(animate);
    render();
    //stats.update();
}

function render() {
    controls.update();
    renderer.render(scene, camera);
}

function linePointTo(line, x, y, z) {
    var geometry = new THREE.Geometry();
    geometry.vertices.push(new THREE.Vector3(0, 0, 0));
    geometry.vertices.push(new THREE.Vector3(x, y, z));
    line.geometry = geometry;
}
function roundRect(ctx, x, y, w, h, r) { ctx.beginPath(); ctx.moveTo(x + r, y); ctx.lineTo(x + w - r, y); ctx.quadraticCurveTo(x + w, y, x + w, y + r); ctx.lineTo(x + w, y + h - r); ctx.quadraticCurveTo(x + w, y + h, x + w - r, y + h); ctx.lineTo(x + r, y + h); ctx.quadraticCurveTo(x, y + h, x, y + h - r); ctx.lineTo(x, y + r); ctx.quadraticCurveTo(x, y, x + r, y); ctx.closePath(); ctx.fill(); ctx.stroke(); }

function makeTextSprite( message, parameters )
{
    if ( parameters === undefined ) parameters = {};
    var fontface = parameters.hasOwnProperty("fontface") ? parameters["fontface"] : "Arial";
    var fontsize = parameters.hasOwnProperty("fontsize") ? parameters["fontsize"] : 36;
    var borderThickness = parameters.hasOwnProperty("borderThickness") ? parameters["borderThickness"] : 4;
    var borderColor = parameters.hasOwnProperty("borderColor") ?parameters["borderColor"] : { r:0, g:0, b:0, a:0.87 };
    var backgroundColor = parameters.hasOwnProperty("backgroundColor") ?parameters["backgroundColor"] : { r:255, g:255, b:255, a:0.87 };
    var textColor = parameters.hasOwnProperty("textColor") ?parameters["textColor"] : { r:0, g:0, b:0, a:1.0 };

    var canvas = document.createElement('canvas');
    var context = canvas.getContext('2d');
    context.font = "Bold " + fontsize + "px " + fontface;
    var metrics = context.measureText( message );
    var textWidth = metrics.width;

    context.fillStyle   = "rgba(" + backgroundColor.r + "," + backgroundColor.g + "," + backgroundColor.b + "," + backgroundColor.a + ")";
    context.strokeStyle = "rgba(" + borderColor.r + "," + borderColor.g + "," + borderColor.b + "," + borderColor.a + ")";

    context.lineWidth = borderThickness;
    roundRect(context, borderThickness/2, borderThickness/2, (textWidth + borderThickness) * 1.1, fontsize * 1.4 + borderThickness, 8);

    context.fillStyle = "rgba("+textColor.r+", "+textColor.g+", "+textColor.b+", 1.0)";
    context.fillText( message, borderThickness, fontsize + borderThickness);

    var texture = new THREE.Texture(canvas) 
    texture.needsUpdate = true;

    var spriteMaterial = new THREE.SpriteMaterial( { map: texture, useScreenCoordinates: false } );
    var sprite = new THREE.Sprite( spriteMaterial );
    sprite.scale.set(0.5 * fontsize, 0.25 * fontsize, 0.75 * fontsize);
    return sprite;  
}

var socket = null;
$(document).ready(function () {
    // Use a "/test" namespace.
    // An application can open a connection on multiple namespaces, and
    // Socket.IO will multiplex all those connections on a single
    // physical channel. If you don't care about multiple channels, you
    // can set the namespace to an empty string.
    namespace = '/positioning';
    // Connect to the Socket.IO server.
    // The connection URL has the following format:
    //     http[s]://<domain>:<port>[/<namespace>]
    socket = io.connect('http://127.0.0.1:6801' + namespace);

    // Event handler for server sent data.
    // The callback function is invoked whenever the server emits data
    // to the client. The data is then displayed in the "Received"
    // section of the page.
    socket.on('objects_update', function (msg) {
        try {
            for (var i in objectsThreeObject) {
                if (msg.data.filter(o => o.oid == i).length > 0)
                    continue
                scene.remove(objectsThreeObject[i]);
            }

            //console.log(msg);
            for (var i in msg.data) {
                var obj = msg.data[i]
                if (obj.oid in objectsThreeObject) {
                    var object = objectsThreeObject[obj.oid];
                    object.position.x = obj.pos[0] / 10;
                    object.position.y = obj.pos[1] / 10;
                    object.position.z = obj.pos[2] / 10;
                    object.scale.x = obj.size[1]/10;
                    object.scale.y = obj.size[1]/10;
                    object.scale.z = obj.size[0]/10;
                    object.children[0].scale.x =0.5*20/ object.scale.x ;
                    object.children[0].scale.y =0.25*20/object.scale.y ;
                    object.children[0].scale.z =0.75* 20/object.scale.z ;
                } else {
                    var geometry = new THREE.BoxGeometry(1, 1, 1);
                    var object = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial({
                        color: Math.random() * 0xffffff,
                        wireframe: true
                    }));
                    object.position.x = obj.pos[0] / 10;
                    object.position.y = obj.pos[1] / 10;
                    object.position.z = obj.pos[2] / 10;
                    object.matrixAutoUpdate = true;

                    object.scale.x = obj.size[1]/10;
                    object.scale.y = obj.size[1]/10;
                    object.scale.z = obj.size[0]/10;
                    objectsThreeObject[obj.oid] = object;
                    var text = makeTextSprite(obj.name+" "+obj.oid)
                    object.add(text)
                    object.children[0].scale.x =0.5*20/ object.scale.x ;
                    object.children[0].scale.y =0.25*20/object.scale.y ;
                    object.children[0].scale.z =0.75* 20/object.scale.z ;
                    scene.add(object);
                }
            }
        } catch (e) {
            console.log(e)
        }

        //$('#log').append('<br>' + $('<div/>').text('Received #' + msg.count + ': ' + msg.data).html());
    });


    socket.on('ar_pos_update', function (msg) {
        try {
            //console.log(msg);
            for (var i in msg.data) {
                if (!Object.keys(msg.data).length)
                    return
                console.log(msg.data)
                name = i
                if (name in devicesThreeObject) {
                    var rot = msg.data[name].rvec;
                    var object = devicesThreeObject[name];

                    var pos = msg.data[name].pos;
                    object.position.x = pos[0] / 10;
                    object.position.y = pos[1] / 10;
                    object.position.z = pos[2] / 10;
                    //console.log(tvec)
                    //console.log(pos)
                    object.rotation.x = -rot[0];
                    object.rotation.y = -rot[1];
                    object.rotation.z = -rot[2];

                    var zero = new THREE.Vector3(0, 0, 0);
                    var qr = new THREE.Vector3(pos[0], pos[1], pos[2]);
                    //qr.applyEuler(new THREE.Euler( rot[0], rot[1], rot[2], 'XYZ' ));
                    //qr.dot(zero)
                    qr.x = qr.x
                    qr.y = qr.y
                    qr.z = qr.z
                    console.log(qr)
                    console.log("distance: " + qr.distanceTo(zero))
                    alpha = 0.7
                    // object.position.x =object.position.x *alpha+ qr.x*(1-alpha);
                    // object.position.y = object.position.y *alpha+qr.y*(1-alpha);
                    // object.position.z =object.position.z *alpha+ qr.z*(1-alpha);
                    qr = object.position
                    linePointTo(linex, qr.x, 0, 0)
                    linePointTo(liney, 0, qr.y, 0)
                    linePointTo(linez, 0, 0, qr.z)
                    linePointTo(lined, qr.x, qr.y, qr.z)

                    //camera.position.set(0,0,0);
                    //camera.lookAt(object.position);
                } else {
                    var pos = msg.data[name].pos;
                    var rvec = msg.data[name].rvec;
                    var geometry = new THREE.BoxGeometry(10, 10, 1);
                    var object = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial({
                        color: Math.random() * 0xffffff
                    }));
                    object.position.x = pos[0] / 10;
                    object.position.y = pos[1] / 10;
                    object.position.z = pos[2] / 10;

                    object.rotation.x = rvec[0];
                    object.rotation.y = rvec[1];
                    object.rotation.z = rvec[2];

                    object.scale.x = 1;
                    object.scale.y = 1;
                    object.scale.z = 1;
                    object.matrixAutoUpdate = true;

                    devicesThreeObject[name] = object;
                    object.add(new THREE.AxesHelper(20))
                    scene.add(object);
                }
            }
        } catch (e) {
            console.log(e)
        }
        //$('#log').append('<br>' + $('<div/>').text('Received #' + msg.count + ': ' + msg.data).html());
    });
});