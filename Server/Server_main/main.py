# main.py
import numpy as np
import cv2
from flask import Flask, render_template, Response, request, redirect, url_for,  jsonify
from object_detection.detector import Detector
from werkzeug.utils import secure_filename
import sys
import os
import json
from collections import namedtuple
import threading
from flask_socketio import SocketIO, emit, join_room, leave_room
from scipy.linalg import expm
from datetime import datetime
from vision import ObjectIdentifier, ObjectLocator, CameraSetting, VisionCore
import traceback
from device import DeviceManager

UPLOAD_FOLDER = 'C:/temp/flask'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])

app = Flask(__name__)
app.config['SECRET_KEY'] = 'tsquare'

deviceDict = DeviceManager.deviceDict
socketio = SocketIO(app)

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
detector = Detector()
#objectLocator = ObjectLocator()
#objectIdentifier = ObjectIdentifier()
detector.start()
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def rot_euler(v, xyz):
    ''' Rotate vector v (or array of vectors) by the euler angles xyz '''
    for theta, axis in zip(xyz, np.eye(3)):
        v = np.dot(np.array(v), expm(np.cross(np.eye(3), axis*-theta)))
    return v

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/ping')
def ping():
    return Response('pong', mimetype='text/plain')

@app.route('/arpos')
def arpos():
    return render_template('arpos.html')

@app.route('/feed',methods=['POST'])
def analysis():
    if request.method == 'POST':
        deviceId = request.args.get('did')
        distance = int(request.args.get('distance'))
        deviceType = request.args.get('type')
        rotatey = float(request.args.get('ry'))
        rotatez = float(request.args.get('rx'))
        rotation =  [rotatey, rotatez]
        location = posDict.get(deviceId, [0,0,0])
        if(len(deviceId)>0):
            print("analysis call by {}".format(deviceId))
        print('going to analysis')
        start = datetime.now()
        r = request
        # convert string of image data to uint8
        nparr = np.fromstring(r.data, np.uint8)
        cameraSetting = CameraSetting.getByName(deviceType)
        if(deviceType == "robot"):
            # decode image
            img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
            checkResult =  detector.check(img)
            print(checkResult)
            if(distance>0):
                checkResult = ObjectLocator.locateCenter(checkResult, location,rotation,cameraSetting, distance)
            checkResult = ObjectIdentifier.identify(checkResult, location, rotation,cameraSetting)
            checkResult = ObjectLocator.locate(checkResult, location,rotation,cameraSetting)
            on_objects_update()
        elif(deviceType=="mobile"):
            checkResult = []
            
        json = jsonify(checkResult)
        print(datetime.now() - start)
        if(len(deviceId)>0):
            print("END analysis call by {}".format(deviceId))
        return json
        
@app.route('/get_avail_device')
def getAvailDevice():
    jsonObj = [{'did': k, 'ip': v.ip} for k, v in deviceDict.items()]
    result = {'status':0, 'data': jsonObj}
    return json.dumps(result)
    
@app.route('/update_ar_pos',methods=['POST'])
def update_ar_pos():
    #if request.method == 'POST':
    try:
        r = request
        jsonObj = json.loads(r.get_json())
        did = jsonObj['did']
        print(jsonObj)
        #jsonObj['lastUpdate'] = datetime.now()
        #jsonObj['tvec'] = rot_euler(jsonObj['tvec'], jsonObj['rvec']).tolist()
        
        DeviceManager.posDict[did] = jsonObj
        result = {'status':0}
        on_ar_pos_update()
    except Exception as e:
        traceback.print_exc()
        result = {'status':-1}
    return json.dumps(result)
#@socketio.on('ar_pos_update', namespace='/positioning')
def on_ar_pos_update():
    jsonObj = DeviceManager.posDict
    result = {'status':0, 'data': jsonObj} 
    socketio.emit('ar_pos_update', result, namespace='/positioning', broadcast=True)
    #app.logger.info(result)

def on_objects_update():
    jsonObj = [(x.__dict__) for x in VisionCore.getInstance().objectList]
    result = {'status':0, 'data': jsonObj} 
    socketio.emit('objects_update', result, namespace='/positioning', broadcast=True)

@app.route('/get_ar_pos',methods=['POST'])
def get_ar_pos():
    #if request.method == 'GET':
    jsonObj = DeviceManager.posDict
    result = {'status':0, 'data': jsonObj}
    return json.dumps(result)

@socketio.on('feed', namespace='/robot')
def on_robot_feed(json):
    did = json['did'] 
    if(did not in deviceDict):
        deviceDict[did] = DeviceManager(detector, did, socketio, request.sid)
        deviceDict[did].ip =  request.headers["X-Real-IP"]
        deviceDict[did].start()
        deviceDict[did].feed(json)
    else:
        deviceDict[did].sid = request.sid
        deviceDict[did].feed(json)
    #socketio.emit('on_update_in_sight', "json", namespace='/robot', broadcast = True)
    #socketio.emit('received_feed', "good", namespace='/robot', broadcast = True)
    #print(F"received from {json['did']}")

@socketio.on('feed_ar', namespace='/robot')
def on_phone_feed_ar(json):
    print("feed_ar")
    print(json)
    did = json['did'] 
    if(did not in deviceDict):
        deviceDict[did] = DeviceManager(detector, did, socketio, request.sid)
        deviceDict[did].ip =  request.headers["X-Real-IP"]
        deviceDict[did].start()
        deviceDict[did].feedArMarker(json['markers'])
    else:
        deviceDict[did].sid = request.sid
        deviceDict[did].feedArMarker(json['markers'])

@socketio.on('feed_objects', namespace='/robot')
def on_phone_feed_objects(json):
    print("feed_objects")
    print(json)
    did = json['did'] 
    if(did not in deviceDict):
        deviceDict[did] = DeviceManager(detector, did, socketio, request.sid)
        deviceDict[did].ip =  request.headers["X-Real-IP"]
        deviceDict[did].start()
        deviceDict[did].feedObjects(json)
    else:
        deviceDict[did].sid = request.sid
        deviceDict[did].feedObjects(json)

@socketio.on('connect', namespace='/robot')
def test_connect():
    join_room(request.sid)
    print(F"{request.sid} joined room")

@socketio.on('reconnect', namespace='/robot')
def test_reconnect():
    join_room(request.sid)
    print(F"{request.sid} joined room")

@socketio.on('disconnect', namespace='/robot')
def test_disconnect():
    leave_room(request.sid)
    print(F"{request.sid} leaved room")

VisionCore.getInstance().start()
if __name__ == '__main__':
    try:
        socketio.run(app, host='0.0.0.0', port = 5801, debug=False)
    except:
        pass
        
VisionCore.getInstance().stop()
for k, v in deviceDict.items():
    v.stop()