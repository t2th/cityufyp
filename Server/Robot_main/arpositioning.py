import sys
import os
import cv2
from cv2 import aruco
import numpy as np
import math
import numpy.linalg as la
import json
import requests
import threading
import time
import scipy.linalg as sla

import traceback
class ArPositioner(object):
    def __init__(self, core, url, arid):
        self.loopThread = None
        self.core = core
        self.arid = arid
        self.url = url
        self.isConnected = False
        self.cameraMatrix = np.load('c270.npz')
        self.mtx, self.dist, _, _ = [self.cameraMatrix[i] for i in ('mtx','dist','rvecs','tvecs')] 
        self.aruco_dict = aruco.Dictionary_get( aruco.DICT_6X6_1000 )
        self.markerLength = 10 # Here, our measurement unit is centimetre.	
        self.arucoParams = aruco.DetectorParameters_create()
        w = 640
        h =480
        newcameramtx, roi=cv2.getOptimalNewCameraMatrix(self.mtx,self.dist,(w,h),1,(w,h))
        self.mapx,self.mapy = cv2.initUndistortRectifyMap(self.mtx,self.dist,None,newcameramtx,(w,h),5)

    def start(self):
        self.loopThread = LoopThread(self)
        self.loopThread.start()
        time.sleep(1)


    def analysis(self, image):
        imgRemapped = cv2.remap(image, self.mapx, self.mapy, cv2.INTER_LINEAR, cv2.BORDER_CONSTANT) # for fisheye remapping
        imgRemapped_gray = cv2.cvtColor(imgRemapped, cv2.COLOR_BGR2GRAY)    # aruco.etectMarkers() requires gray image
        corners, ids, rejectedImgPoints = aruco.detectMarkers(imgRemapped_gray, self.aruco_dict, parameters=self.arucoParams) # Detect aruco

        if ids is not None: # if aruco marker detected
            rvecs, tvecs, _ = aruco.estimatePoseSingleMarkers(corners, self.markerLength, self.mtx, self.dist) # For a single marker
            #imgWithAruco = aruco.drawDetectedMarkers(imgRemapped, corners, ids, (0,255,0))
            #imgWithAruco = aruco.drawAxis(imgWithAruco, self.mtx, self.dist, rvecs, tvecs, 10)    # axis length 100 can be changed according to your requirement
            result = {'did':self.core.id}
            listMarker = []
            listId = []
            for o in ids: listId.append(o[0])
            for i in range(len(listId)): 
                listMarker.append({
                    'id': int(listId[i]),
                    'tvec': list(tvecs[i][0]),
                    'rvec': list(rvecs[i][0])
                    })
            result['markers'] = listMarker

            print(listMarker)
            if 2 in listId:
                idx = listId.index(2)
                try:
                    _r  = cv2.Rodrigues(rvecs[idx])[0]
                    _r2 = rotationMatrixToEulerAngles(_r)
                    pos = rot_euler(tvecs[idx][0], _r2)
                    pos[2] = -pos[2]
                    print(pos)
                    des = tvecs[idx][0]
                    result['pos'] = pos.tolist() #self position
                    result['tvec'] = des.tolist()
                    result['rvec'] = _r2.tolist() #self rotation
                    s = json.dumps(result)
                    self.core.position = pos
                    self.core.arrotation = _r2
                    try:
                        callUrl ="{}/update_ar_pos".format(self.url)
                        print(callUrl)
                        res = requests.post(callUrl, json=s, timeout=1).json()
                        #print(res['status'])
                        if res['status']==0:
                            last_update = current
                    except Exception as e:
                        traceback.print_exc()
                except Exception as e:
                    traceback.print_exc()
                    #print(e)

    def getImage(self):
        #ret, jpeg = cv2.imencode('.jpg', self.loopThread.getFrame())
        #return jpeg.tobytes()
        return self.loopThread.getImage()

    def stop(self):
        self.loopThread.stop()

class LoopThread(threading.Thread):
    def __init__(self,owner):
        threading.Thread.__init__(self, daemon=False, args=())
        self.isStop = False
        self.owner =owner

    def stop(self):
        self.isStop = True    
        
    def run(self):
        print('ar positioning started!')
        while (not self.isStop):
            try:
                img = self.owner.core.camera.getFrame()
                self.owner.latestResult = self.owner.analysis(img)
                time.sleep(0.01)
                if __debug__:
                    pass
                    #print(self.owner.latestResult)
                self.owner.isConnected = True
                
            except Exception as e:
                traceback.print_exc()
                print(e)
                self.owner.isConnected = False
        print('ar positioning stopped!')

def isRotationMatrix(R) :
    Rt = np.transpose(R)
    shouldBeIdentity = np.dot(Rt, R)
    I = np.identity(3, dtype = R.dtype)
    n = np.linalg.norm(I - shouldBeIdentity)
    return n < 1e-6
def rotationMatrixToEulerAngles(R) :
 
    assert(isRotationMatrix(R))
     
    sy = math.sqrt(R[0,0] * R[0,0] +  R[1,0] * R[1,0])
     
    singular = sy < 1e-6
 
    if  not singular :
        x = math.atan2(R[2,1] , R[2,2])
        y = math.atan2(-R[2,0], sy)
        z = math.atan2(R[1,0], R[0,0])
    else :
        x = math.atan2(-R[1,2], R[1,1])
        y = math.atan2(-R[2,0], sy)
        z = 0
 
    return np.array([x, y, z])
    
def rotation_matrix(axis, theta):
    """
    Return the rotation matrix associated with counterclockwise rotation about
    the given axis by theta radians.
    """
    axis = np.asarray(axis)
    axis = axis/math.sqrt(np.dot(axis, axis))
    a = math.cos(theta/2.0)
    b, c, d = -axis*math.sin(theta/2.0)
    aa, bb, cc, dd = a*a, b*b, c*c, d*d
    bc, ad, ac, ab, bd, cd = b*c, a*d, a*c, a*b, b*d, c*d
    return np.array([[aa+bb-cc-dd, 2*(bc+ad), 2*(bd-ac)],
                     [2*(bc-ad), aa+cc-bb-dd, 2*(cd+ab)],
                     [2*(bd+ac), 2*(cd-ab), aa+dd-bb-cc]])
def rot_euler(v, xyz):
    ''' Rotate vector v (or array of vectors) by the euler angles xyz '''
    #https://stackoverflow.com/questions/6802577/rotation-of-3d-vector
    for theta, axis in zip(xyz, np.eye(3)):
        v = np.dot(np.array(v), sla.expm(np.cross(np.eye(3), axis*-theta)))
    return v
    
def getRTfromMatrix44 ( M) :
    u,s,vh = np.linalg.svd(M, full_matrices=True)
    rpure = u * vh
    R = cv2.Rodrigues(rpure)
    T = np.array(([M[0, 3], M[1,3], M[2,3]]))
    print(R)
    print(T)
    return R, T
def cyl2cat(r, theta, z):
    return (r*np.cos(theta), r*np.sin(theta), z)