
import threading
import serial
import json
import time
import body.modelSelector

class ArduinoService(object):
    def __init__(self, core, port):
        self.core = core
        self.loopThread = None
        self.port =port
        self.isConnected = False
        self.modelDriver = None
    
    
    def start(self):
        self.loopThread = LoopThread(self, self.port)
        self.loopThread.start()
        time.sleep(1)

    def stop(self):
        self.loopThread.stop()

class LoopThread(threading.Thread):
    def __init__(self, owner, port):
        threading.Thread.__init__(self, daemon=False, args=())
        self.isStop = False
        self.owner = owner
        self.port = port
        self.ser = None

    def stop(self):
        self.isStop = True
           
    def run(self):
        print('serial on '+self.port+' started!')
        while (not self.isStop):
            try:
                if __debug__:
                    print('serial on '+self.port+' trying to connect!')
                self.ser = serial.Serial(self.port, 9600, timeout = 0.5)
                #self.ser.open()
                print('serial on '+self.port+' connected!')
                self.owner.isConnected = True
                while (not self.isStop):
                    raw = self.ser.readline()
                    line = raw.decode("ascii")
                    try:
                        data = json.loads(line)
                        if __debug__:
                            pass
                            #print(data)
                        if self.owner.modelDriver is None:
                            tmpModel = body.modelSelector.getModel(data["_m"])
                            tmpModel.ser = self.ser
                            if(tmpModel is None):
                                if(__debug__):
                                    print("unkown model: "+data["_m"])
                            else:
                                print("bined to model: "+data["_m"])
                                self.owner.modelDriver =tmpModel
                                self.owner.modelDriver.feedData(data)
                        else:
                            self.owner.modelDriver.feedData(data)
                    except Exception as e:
                        self.owner.modelDriver = None
            except:
                self.owner.modelDriver = None
                if(self.owner.isConnected):
                    print('serial on '+self.port+' disconnected!')
                self.owner.isConnected = False
                time.sleep(1) #re try to connect to a body
        try:
            if(self.ser is not None):
                self.ser.close()
        except:
            pass
        print('serial on '+self.port+' stopped!')