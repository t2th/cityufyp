from .actionbase import ActionBase

class StopAction(ActionBase):
    def getName(self):
        return F"Stop Action"

    def __init__(self, core, data):
        super(StopAction, self).__init__(core)
        self.isForce = True
    
    def run(self):
        self.isStarted = True
        print("ACTION: StopAction started")
        self.core.stopAction()
        print("ACTION: StopAction stopped")