from .actionbase import ActionBase
import time
import numpy as np
import numpy.linalg as la
import math
class GotoAction(ActionBase):
    
    def getName(self):
        return F"Go to {self.targetId}"

    def __init__(self, core, data):
        super(GotoAction, self).__init__(core)
        #self.targetName= data["name"]
        self.targetId = data["oid"]
    
    def run(self):
        self.isStarted = True
        print("ACTION: GotoAction started")
        model = self.core.getModel()
        counter = 0
        while(model is not None and not self.isStop):
            if(counter >10):
                counter = 0
            counter +=1
            time.sleep(0.1)
            objs = [x for x in self.core.getObjectList() if x["oid"] == self.targetId]
            if(len(objs) >0 ):
                obj = objs[0]
                cameraPos = self.core.getPosition()
                coreRvec = self.core.getRvec()
                cameraMat = createRotationMatrix(coreRvec[2], coreRvec[1], coreRvec[0])
                objAngle = angleBetween(cameraPos, obj["pos"])
                objDistance = distanceBetween3D(cameraPos,  obj["pos"])
                objMat = createRotationMatrix(objAngle[2], objAngle[1], objAngle[0])
                rotation2Obj = np.matmul(objMat, np.transpose(cameraMat))
                rotationEuler = rotationMatrixToEulerAngles(rotation2Obj).tolist()
                print(rotationEuler)
                continue 
                if(rotationEuler[0]>0.2):
                    model.right()
                    if(counter >20):
                        model.doFace(faceDict["lookRight"])
                elif(rotationEuler[0]<-0.2):
                    model.left()
                    if(counter >20):
                        model.doFace(faceDict["lookLeft"])
                elif(objDistance>100):
                    model.forward()
                    if(counter >20):
                        model.doFace(faceDict["working"])
                else:
                    model.stop()
                    self.stop()
                
        model.doFace(faceDict["smile"])
        print("ACTION: GotoAction stopped")
        
        self.core.startNextAction()

def normalizeRadian(i):
    if(i<0):
        i = (-i)% (math.pi * 2)
        return (math.pi * 2)-i
    i = i% (math.pi * 2)
    return i
def angleBetween(v1, v2):
    x = v1[0]-v2[0]
    y = v1[1]-v2[1]
    z = v1[2]-v2[2]
    yaw = math.atan2(x, z) 
    padj =  math.sqrt(x**2) + z**2
    pitch =  math.atan2(padj, y) 
    return [yaw,pitch, 0]

def createRotationMatrix(yaw, pitch, roll):
    yawMatrix = np.matrix([
    [math.cos(yaw), -math.sin(yaw), 0],
    [math.sin(yaw), math.cos(yaw), 0],
    [0, 0, 1]
    ])

    pitchMatrix = np.matrix([
    [math.cos(pitch), 0, math.sin(pitch)],
    [0, 1, 0],
    [-math.sin(pitch), 0, math.cos(pitch)]
    ])

    rollMatrix = np.matrix([
    [1, 0, 0],
    [0, math.cos(roll), -math.sin(roll)],
    [0, math.sin(roll), math.cos(roll)]
    ])

    return yawMatrix * pitchMatrix * rollMatrix
def distanceBetween3D(p1, p2):
    return np.sqrt((p2[0]-p1[0])**2 +(p2[1]-p1[1])**2 +(p2[2]-p1[2])**2 )

def rotationMatrixToEulerAngles(R) :
    sy = math.sqrt(R[0,0] * R[0,0] +  R[1,0] * R[1,0])
     
    singular = sy < 1e-6
 
    if  not singular :
        x = math.atan2(R[2,1] , R[2,2])
        y = math.atan2(-R[2,0], sy)
        z = math.atan2(R[1,0], R[0,0])
    else :
        x = math.atan2(-R[1,2], R[1,1])
        y = math.atan2(-R[2,0], sy)
        z = 0
 
    return np.array([x, y, z])
