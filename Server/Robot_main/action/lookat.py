from .actionbase import ActionBase
import time
from .faces import faceDict
class LookAtAction(ActionBase):
    
    def getName(self):
        return F"Look At {self.targetId}"

    def __init__(self, core, data):
        super(LookAtAction, self).__init__(core)
        #self.targetName= data["name"]
        self.targetId = data["oid"]
    
    def run(self):
        self.isStarted = True
        print("ACTION: LookAtAction started")
        model = self.core.getModel()
        model.doFace(faceDict["working"])
        counter = 0
        while(model is not None and not self.isStop):
            if(counter >10):
                counter = 0
            counter +=1
            notFoundCounter = 0
            time.sleep(0.05)
            found = False
            for it in self.core.getObjectInSight():
                #if(it["name"]==self.targetName):
                if(it["oid"]==self.targetId):
                    found = True
                    box = it["box"]
                    print("{}, {}, {}".format((box[3]-box[1] ), (box[2]-box[0]), self.core.getDistance()))
                    ax = (box[1]+box[3])/2
                    noMove = False
                    if(ax<0.4):
                        model.yawHeadToLeft()
                        if(counter >20):
                            model.doFace(faceDict["lookLeft"])
                    elif(ax > 0.6):
                        model.yawHeadToRight()
                        if(counter >20):
                            model.doFace(faceDict["lookRight"])
                    else:
                        noMove = True
                    ay = (box[0]+box[2])/2
                    if(ay<0.4):
                        model.pitchHeadToUp()
                    elif(ay>0.6):
                        model.pitchHeadToDown()
                    else:
                        if(noMove and counter >20):
                            model.doFace(faceDict["working"])
            if(not found):
                notFoundCounter+=1
                if(notFoundCounter % 10==0):
                    model.yawHeadStop()
                    model.pitchHeadStop()
                    model.doFace(faceDict["working"])

                if(notFoundCounter>200):
                    self.stop()
            else:
                notFoundCounter = 0

        model.headReset()
        model.doFace(faceDict["smile"])
        print("ACTION: LookAtAction stopped")
        
        self.core.startNextAction()