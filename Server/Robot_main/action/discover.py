from .actionbase import ActionBase
import time
from .faces import faceDict
class DiscoverAction(ActionBase):
    
    def getName(self):
        return "Discover"

    def __init__(self, core, data):
        super(DiscoverAction, self).__init__(core)
    
    def run(self):
        self.isStarted = True
        print("ACTION: DiscoverAction started")
        model = self.core.getModel()
        model.doFace(faceDict["working"])
        counter = 0
        while(model is not None and not self.isStop):
            if(counter >10):
                counter = 0
            counter +=1
            notFoundCounter = 0
            time.sleep(0.05)
            found = False
            for it in self.core.getObjectInSight():
                #if(it["name"]==self.targetName):
                if(it["oid"]==""):
                    found = True
                    box = it["box"]
                    print("{}, {}, {}".format((box[3]-box[1] ), (box[2]-box[0]), self.core.getDistance()))
                    ax = (box[1]+box[3])/2
                    noMove = False
                    if(ax<0.4):
                        model.yawHeadToLeft()
                        if(counter >20):
                            model.doFace(faceDict["lookLeft"])
                    elif(ax > 0.6):
                        model.yawHeadToRight()
                        if(counter >20):
                            model.doFace(faceDict["lookRight"])
                    else:
                        noMove = True
                    ay = (box[0]+box[2])/2
                    if(ay<0.4):
                        model.pitchHeadToUp()
                    elif(ay>0.6):
                        model.pitchHeadToDown()
                    else:
                        if(noMove and counter >20):
                            model.doFace(faceDict["working"])
            if(not found):
                notFoundCounter+=1
                if(notFoundCounter % 10==0):
                    model.headReset()
                    model.doFace(faceDict["working"])
            else:
                notFoundCounter = 0
                                 
        model.headReset()
        model.doFace(faceDict["smile"])
        print("ACTION: DiscoverAction stopped")
        
        self.core.startNextAction()