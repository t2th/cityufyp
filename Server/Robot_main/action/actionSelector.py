from .lookat import LookAtAction
from .stopaction import StopAction
from .discover import DiscoverAction
from .goto import GotoAction
actions = {
    'lookat':'LookAtAction',
    'stopaction':'StopAction',
    'discover':'DiscoverAction',
    'goto':'GotoAction'
}
def createAction(core, data):
    name = data["action"]
    if(name in actions):
        return globals()[actions[name]](core, data)#bodyModel[modelName]
    return None