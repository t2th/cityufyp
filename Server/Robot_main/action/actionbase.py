import abc
import serial
import threading

class ActionBase(threading.Thread):
    def __init__(self, core):
        threading.Thread.__init__(self, daemon=False, args=())
        self.isStop = False
        self.core = core
        self.nextAction = None
        self.isStarted = False
        self.isForce = False


    @abc.abstractmethod
    def run(self):
        pass


    def stop(self):
        self.isStop = True


    @abc.abstractmethod
    def getName(self):
        pass
