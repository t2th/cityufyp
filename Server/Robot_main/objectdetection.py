import requests
import json
import threading
import time
from flask_socketio import SocketIO, emit
class ObjectDetection(object):
    def __init__(self, core, url):
        self.loopThread = None
        self.core = core
        self.url = url
        self.isConnected = False
        self.latestResult = []

    
    def start(self):
        self.loopThread = LoopThread(self)
        self.loopThread.start()
        time.sleep(1)


    def analysis(self, image):
        if(self.core.isReadyForDetection()):
            rotation = self.core.getRotation()
            if rotation is None: rotation = {"ry": 0, "rx": 0}
            callUrl = "{}/feed?did={}&distance={}&type=robot&ry={}&rx={}".format(
                self.url, self.core.id, self.core.getDistance() , rotation["ry"], rotation["rx"]
            )
        else:
            callUrl = "{}/feed?did={}&distance={}&type=robot&ry={}&rx={}".format(
                self.url, self.core.id, 100, 0, 0
            )
        print(callUrl)
        data = image 
        response = requests.post(callUrl, data=data, timeout=1)
        json_data = json.loads(response.text)
        return json_data 

    def getImage(self):
        #ret, jpeg = cv2.imencode('.jpg', self.loopThread.getFrame())
        #return jpeg.tobytes()
        return self.loopThread.getImage()

    def stop(self):
        self.loopThread.stop()

class LoopThread(threading.Thread):
    def __init__(self,owner):
        threading.Thread.__init__(self, daemon=False, args=())
        self.isStop = False
        self.owner =owner

    def stop(self):
        self.isStop = True    
        
    def run(self):
        print('object detection started!')
        while (not self.isStop):
            try:
                img = self.owner.core.camera.getImage()
                self.owner.latestResult = self.owner.analysis(img)
                self.owner.core.socketio.emit('detectedObject', self.owner.latestResult, room="controller")
                if __debug__:
                    pass
                    #print(self.owner.latestResult)
                self.owner.isConnected = True
                
            except Exception as e:
                print(e)
                self.owner.isConnected = False
        print('object detection stopped!')