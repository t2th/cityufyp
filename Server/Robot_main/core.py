
from camera import CameraService
from arduino import ArduinoService
from objectdetection import ObjectDetection
from uuid import getnode as get_mac
#from arpositioning import ArPositioner
import queue
class Core(object):
    def __init__(self, socketio):
        self.socketio = socketio
        self.arduino = None
        self.camera = None
        #self.objectDetection = None
        #self.arPositioning = None
        self.mainServerConnection = None
        self.isStarted = False
        self.isStop = False
        self.currentAction = None
        self.actionQueue = queue.Queue()
        self.id =  "".join(c + "-" if i % 2 else c for i, c in enumerate(hex(get_mac())[2:].zfill(12)))[:-1].upper()
        self.position = [0,0,0]
        self.rvec = None
        self.arrotation = [0,0,0]
        self.objectInSight = []
        self.arid = -1
        self.objectList = []

    def start(self):
        if(self.isStarted):
            return
        self.arduino.start()
        self.camera.start()
       # self.objectDetection.start()
        #self.arPositioning.start()
        self.mainServerConnection.start()
        self.isStarted = True
    
    def stop(self):
        if(not self.isStarted):
            return
        self.mainServerConnection.stop()
        #self.arPositioning.stop()
        #self.objectDetection.stop()
        self.camera.stop()
        self.arduino.stop()
        self.isStarted = False
        self.actionQueue = queue.Queue()
        if(self.currentAction is not None):
            self.currentAction.stop()
    def getPosition(self):
        return self.position
    def getRvec(self):
        return self.rvec
    def getObjectList(self):
        return self.objectList
    def getModel(self):
        return self.arduino.modelDriver
    def isBodyConnected(self):
        return self.arduino.isConnected
    def getObjectInSight(self):
        return self.objectInSight
    def getDistance(self):
        if self.arduino.modelDriver is None:
            return None
        return self.arduino.modelDriver.getFacingDistance()
    def getRotation(self):
        if self.arduino.modelDriver is None:
            return None
        return self.arduino.modelDriver.getVisionDirection()
    def isCameraReady(self):
        return self.camera.isConnected
    def isReadyForDetection(self):
        if((not self.isBodyConnected() )or self.getDistance() is None or self.getRotation() is None):
            return False
        return True


    def queueAction(self, action):
        if(action.isForce):
            action.start()
        else:
            self.actionQueue.put(action)
            if(self.currentAction is None):
                self.startNextAction()


    def startNextAction(self):
        self.currentAction = None
        self.currentAction  = self.actionQueue.get()
        if(self.currentAction is not None):
            self.currentAction.start()

    def stopAction(self):
        if(self.currentAction is not None):
            self.currentAction.stop()