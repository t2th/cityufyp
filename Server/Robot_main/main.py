# main.py
import sys
if(len(sys.argv) <3):
    print("needed two parameters: OS, arid (e.g. python3 main.py windows 12")
    exit()


from flask import Flask, render_template, Response,request, jsonify
from flask_socketio import SocketIO, emit, join_room, leave_room, send
from core import Core
from camera import CameraService
from arduino import ArduinoService
from objectdetection import ObjectDetection
from mainserver import MainServer
import action.actionSelector as actions
import time
import logging
import json
log = logging.getLogger('werkzeug')
#log.setLevel(logging.ERROR)

app = Flask(__name__)
socketio = SocketIO(app)
core = Core(socketio)
core.camera = CameraService(core)
if(sys.argv[1] == "windows"):
    core.arduino = ArduinoService(core, "COM5")
else:
    core.arduino = ArduinoService(core, "/dev/ttyACM0")
core.arid = int(sys.argv[2])
core.mainServerConnection = MainServer.getInstance(core, "http://192.168.1.100", 12)
#core.objectDetection = ObjectDetection(core, "http://192.168.1.100:6801")
#core.arPositioning = ArPositioner(core, "http://192.168.1.100:6801", 12)
@app.route('/')
def index():
    return render_template('index.html')

@app.route('/ping')
def ping():
    return Response('pong', mimetype='text/plain')

def gen(camera):
    while True:
        time.sleep(0.1)
        frame = core.camera.getImage()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')

@app.route('/video_feed')
def video_feed():
    return Response(gen(core.camera),
                    mimetype='multipart/x-mixed-replace; boundary=frame')
                    
@app.route('/capture')
def capture():
    return Response(core.camera.getImage(),
                    mimetype='.ipeg,.jpg image/jpeg; boundary=frame')

#@app.route('/detectedObject')
#def detectedObject():
#    return jsonify(core.objectDetection.latestResult)


@socketio.on('joined')
def joined():
    #room = "controller"
    #join_room(room)
    print("someone entered the room")
    emit('status', {'msg':'has entered the room.'})

@socketio.on('left')
def left():
    #room = "controller"
    #leave_room(room)
    print("someone left the room")

@app.route('/action', methods=['POST'])
def action():
    content = request.get_json()
    print(content)
    act = actions.createAction(core, content)
    if(act is None):
        return jsonify({"status": 1, "msg": "Action not Found"})
    else:
        core.queueAction(act)
        return jsonify({"status": 0, "msg": ""})

@socketio.on('connect')
def test_connect():
    print(F"{request.sid} joined room")

@socketio.on('reconnect')
def test_reconnect():
    print(F"{request.sid} joined room")

@socketio.on('disconnect')
def test_disconnect():
    print(F"{request.sid} leaved room")

@socketio.on('action')
def on_action_requested(json):
    print(json)
    act = actions.createAction(core, json)
    if(act is None):
        print(jsonify({"status": 1, "msg": "Action not Found"}))
    else:
        core.queueAction(act)
        print(jsonify({"status": 0, "msg": ""}))

        
@app.route('/get_action')
def getAvailDevice():
    jsonObj = [ type(v).__name__ for v in list(core.actionQueue.queue)]
    if(core.currentAction is not None):
        jsonObj = [core.currentAction.getName()] + jsonObj
    result = {'actions': jsonObj}
    return json.dumps(result)

core.start()
print("CORE ID: {}".format(core.id))
if __name__ == '__main__':
    try:
        socketio.run(app, host='0.0.0.0',port = 5802, debug=False)
    except:
        print("ending")
try:
    core.stop()
except:
    pass