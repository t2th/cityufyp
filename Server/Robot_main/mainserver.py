from socketio_client import SocketIO, LoggingNamespace, BaseNamespace

import cv2
import sys
import threading
import time
import traceback
import base64
import math

class RobotNamespace(BaseNamespace):

    def on_connect(self):
        print('[Connected]')

    def on_reconnect(self):
        print('[Reconnected]')

    def on_disconnect(self):
        print('[Disconnected]')
    def on_received_feed(self,msg):
        print("received feed")
    def on_update_in_sight(self,json):
        MainServer.getInstance().core.objectInSight = json["results"]
        MainServer.getInstance().core.socketio.emit('detectedObject', json, broadcast=True)
               
        #self.owner.core.socketio.emit('detectedObject', self.owner.latestResult, room="controller")
        print("on_update_in_sight")
    def on_update_ar_position(self, json):
        #self.owner.core.socketio.emit('detectedObject', self.owner.latestResult, room="controller")
        if("pos" in json.keys()):
            print(json["pos"])
            print(json["rvec"])
            MainServer.getInstance().core.position = json["pos"]
            MainServer.getInstance().core.rvec = json["rvec"]
        print("on_update_ar_position")
        #MainServer.getInstance().on_update_ar_position()
    def on_objects_update(self, json):
        data = json["data"]
        MainServer.getInstance().core.objectList = data
        print("on_objects_update")

class MainServer(object):
    _instance = None
    def getInstance(core=None, url="192.168.1.100", arid=12):
        if(MainServer._instance is None):
            MainServer._instance = MainServer(core, url, arid)
        return MainServer._instance
    def __init__(self, core, url, arid):
        self.url = url
        self.core = core
        self.arid = arid
        self.isConnected = False
        self.loopThread = None
        self.io =  SocketIO(url, 6801,  wait_for_connection=False) 
        self.ioRoom = self.io.define(RobotNamespace, '/robot')
        self.socketIO_thread = threading.Thread(target=self.io.wait)
        self.objectList = None
    def start(self):
        #self.io.connect()
        self.socketIO_thread.start()
        self.loopThread = LoopThread(self)
        self.loopThread.start()
        #self.io.wait(seconds=1)
    def stop(self):
        self.loopThread.stop()
        print(F"Disconnecting socket io")
        self.io.disconnect()
        self.socketIO_thread.join()
        print(F"Disconnected socket io")
        #self.io.disconnect()
    def feeding(self):
        c=self.core
        if(self.core.isReadyForDetection()):
            did = c.id
            rotation = c.getRotation()
            ry = (rotation["ry"]-90) * (math.pi /180)
            distance = c.getDistance()
            if(c.camera.uploaded):
                self.ioRoom.emit("feed", {
                    'skipImage':0,
                    'did': did,
                    'pitchDelta': ry,
                    'distance':distance,
                    'arid': c.arid,
                    'type': 'robot'
                })
            else:
                image = c.camera.getImage()
                self.ioRoom.emit("feed", {
                    'image': base64.b64encode(image).decode('utf-8'),
                    'did': did,
                    'pitchDelta': ry,
                    'distance':distance,
                    'arid': c.arid,
                    'type': 'robot'
                })
            c.camera.uploaded = True
        elif self.core.isCameraReady():
            did = c.id
            if(c.camera.uploaded):
                self.ioRoom.emit("feed", {
                    'skipImage':0,
                    'did': did,
                    'arid': c.arid,
                    'type': 'robot'
                })
            elif( c.camera.getImage() is not None):
                image = c.camera.getImage()
                self.ioRoom.emit("feed", {
                    'image': base64.b64encode(image).decode('utf-8'),
                    'did': did,
                    'arid': c.arid,
                    'type': 'robot'
                })
            c.camera.uploaded = True
        else:
            #image = c.camera.getImage()
            did = c.id
            self.ioRoom.emit("feed", {
                'did': did,
                'ry': 0,
                'rx':90,
                'distance':0,
                'arid': c.arid,
                'type': 'robot'
            })
                    
    # def on_robot_connected(self):
    #     print("MainServer socket io connected")
    #     MainServer.getInstance().isConnected = True

    # def on_robot_disconnected(self):
    #     print("MainServer socket io disconnected")
    #     MainServer.getInstance().isConnected = False

    def on_update_in_sight(self,json):
        #self.owner.core.socketio.emit('detectedObject', self.owner.latestResult, room="controller")
        print("on_update_in_sight")

    def on_update_ar_position(self):
        #self.owner.core.socketio.emit('detectedObject', self.owner.latestResult, room="controller")
        print("on_update_ar_position")

class LoopThread(threading.Thread):
    def __init__(self,owner):
        threading.Thread.__init__(self, daemon=False, args=())
        self.isStop = False
        self.owner =owner

    def stop(self):
        self.isStop = True    
        
    def run(self):
        print('main server connection started!')
        while (not self.isStop):
            try:
                self.owner.feeding()
                if __debug__:
                    pass
                    #print(self.owner.latestResult)
                self.owner.isConnected = True
                
            except Exception as e:

                traceback.print_exc()
                self.owner.isConnected = False
            finally:
                time.sleep(0.035)
        print('main server connection stopped!')