# camera.py

import cv2
import sys
import threading
import time
import traceback
class CameraService(object):
    def __init__(self, core):
        self.core = core
        self.loopThread = None
        self.frame = None
        self.isConnected = False
        self.uploaded = False
    
    
    def start(self):
        self.loopThread = LoopThread(self, 0)
        self.loopThread.start()
        time.sleep(1)

    def getImage(self):
        #ret, jpeg = cv2.imencode('.jpg', self.loopThread.getFrame())
        #return jpeg.tobytes()
        return self.loopThread.getImage()
    def getFrame(self):
        return self.loopThread.frame

    def stop(self):
        self.loopThread.stop()

class LoopThread(threading.Thread):
    def __init__(self,owner, url):
        threading.Thread.__init__(self, daemon=False, args=())
        self.image = None
        self.frame = None
        self.status = False
        self.isStop = False
        self.url = url
        self.capture = None
        self.owner =owner

    def stop(self):
        self.isStop = True
   
    def getImage(self):
        return self.image
    
        
    def run(self):
        strUrl = str(self.url)
        print('ipcam on '+strUrl+' started!')
        while (not self.isStop):
            try:
                if __debug__:
                    print('ipcam on '+strUrl+' trying to connect!')
                self.capture = cv2.VideoCapture(self.url)
                self.owner.isConnected = True
                print('ipcam on '+strUrl+' connected!')
                time.sleep(0.5)
                while (not self.isStop):
                    self.status, frame = self.capture.read()
                    ret, jpeg = cv2.imencode('.jpg', frame)
                    self.frame = frame
                    self.image = jpeg.tobytes()
                    self.owner.uploaded = False
                    time.sleep(0.035)
            except:
                if(self.isStop):
                    break
                if(self.capture is not None):
                    self.capture.release()
                if(self.owner.isConnected):
                    print('ipcam on '+strUrl+' disconnected!')
                self.owner.isConnected = False
                time.sleep(3) #re try to connect to a camera
        if(self.capture is not None):
            try:
                self.capture.release()
            except:
                pass
        print('ipcam on '+strUrl+' stopped!')
