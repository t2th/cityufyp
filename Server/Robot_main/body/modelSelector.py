from .car01 import Car01


bodyModel = {
    'car01':'Car01'
}
def getModel(modelName):
    if modelName in bodyModel:
        return globals()[bodyModel[modelName]]()#bodyModel[modelName]
    return None