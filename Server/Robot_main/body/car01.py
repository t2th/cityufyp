from .bodybase import BodyBase

class Car01(BodyBase):
    def __init__(self):
        super(Car01,self).__init__()
        self.servoBotRotation = 0
        self.servoTopRotation =0
        self.servoHandRotation = 0
        self.lastMovementCmd =" "
        self.headingDegree = 0
        self.laserDistance = 0
        self.lastServoBotRotation = -2
        self.lastServoTopRotation =  -2
        print("car01 created")

    def feedData(self, data):
        self.lastServoBotRotation = data["s1"]
        self.lastServoTopRotation = data["s2"]
        self.servoHandRotation = data["sh"]
        self.lastMovementCmd = data["a"]
        self.headingDegree = data["deg"]
        self.laserDistance = data["ld"]
    
    def forward(self):
        self.sendCommand("w")
    def backward(self):
        self.sendCommand("d")
    def left(self):
        self.sendCommand("a")
    def right(self):
        self.sendCommand("d")
    def stop(self):
        self.sendCommand(" ")
    def yawHead(self, angle): #left right
        if( self.lastServoBotRotation == int(angle)):
            return
        self.lastServoBotRotation = int(angle)
        self.sendCommand("q "+str(int(angle)))
    def pitchHead(self, angle): #up down
        if( self.lastServoTopRotation == int(angle)):
            return
        self.lastServoTopRotation = int(angle)
        self.sendCommand("e "+str(int(angle)))
    def doFace(self, faceString):
        self.sendCommand("f "+faceString)

    def yawHeadStop(self):
        self.yawHead(-1)
    def pitchHeadStop(self):
        self.pitchHead(-1)
    def yawHeadToLeft(self):
        self.yawHead(180)
    def yawHeadToRight(self):
        self.yawHead(0)
        
    def pitchHeadToUp(self):
        self.pitchHead(0)
    def pitchHeadToDown(self):
        self.pitchHead(180)
    def headReset(self):
        self.yawHead(90)
        self.pitchHead(90)
    def getFacingDistance(self):
        return self.laserDistance
        
    def getVisionDirection(self):
        return {"ry": self.headingDegree + self.lastServoBotRotation, "rx": self.lastServoTopRotation}