import abc
import serial
import threading

class BodyBase(object):
    def __init__(self):
        self.ser = None
        self.serialWriteLock = threading.Lock()
    @abc.abstractmethod
    def feedData(self, data):
        pass
    
    def sendCommand(self, cmd):
        if(self.ser is not None):
            try:
                self.serialWriteLock.acquire()
                if __debug__:
                    print("going to send "+cmd)
                self.ser.write((cmd+"\n").encode())
                self.serialWriteLock.release()
                
            except Exception as e:
                print(e)

    
    @abc.abstractmethod
    def getFacingDistance(self):
        pass
    @abc.abstractmethod
    def getVisionDirection(self):
        pass