package com.ttommy.robot_controller_android;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ttommy.robot_controller_android.connection.BluetoothHelper;
import com.ttommy.robot_controller_android.connection.Request;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;


public class DirectControlFragment extends Fragment {
    private static final String TAG = "DirectControl";
    private View view;
    private Button btnForward, btnLeft, btnBack, btnRight;
    private Hashtable<Integer, String> idToDirectionName = new Hashtable<Integer, String>(){
        {
            put(R.id.btnForward, "Forward");
            put(R.id.btnLeft, "Left");
            put(R.id.btnBack, "Back");
            put(R.id.btnRight, "Right");
        }
    };


    public DirectControlFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_direct_control, container, false);
        layoutSetup();
        return view;
    }

    private void layoutSetup(){
        btnForward =  view.findViewById(R.id.btnForward);
        btnBack = view.findViewById(R.id.btnBack);
        btnLeft = view.findViewById(R.id.btnLeft);
        btnRight = view.findViewById(R.id.btnRight);

        View.OnTouchListener sharedListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return directionButtonOnTouch(view, motionEvent);
            }
        };

        btnForward.setOnTouchListener(sharedListener);
        btnBack.setOnTouchListener(sharedListener);
        btnLeft.setOnTouchListener(sharedListener);
        btnRight.setOnTouchListener(sharedListener);
    }

    private boolean directionButtonOnTouch(View view, MotionEvent event){
        Gson gson = new GsonBuilder().create();

        if(event.getAction() == MotionEvent.ACTION_DOWN) {
            Request request = new Request();
            request.subject = idToDirectionName.get(view.getId());
            request.content.put("status", "DOWN");
            BluetoothHelper.getInstance(getContext()).sendBtMsg(
                    gson.toJson(request)
            );
            Log.d(TAG, "DOWN: "+idToDirectionName.get(view.getId()));
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            Request request = new Request();
            request.subject = idToDirectionName.get(view.getId());
            request.content.put("status", "UP");
            BluetoothHelper.getInstance(getContext()).sendBtMsg(
                    gson.toJson(request)
            );
        }
        return true;
    }
}
