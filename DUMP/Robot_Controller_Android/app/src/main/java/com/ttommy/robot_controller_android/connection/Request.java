package com.ttommy.robot_controller_android.connection;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by TTommy on 10/5/2017.
 */

public class Request {
    public static int counter = 0;
    public Request(){
        id=counter++;
    }
    public int id;
    public String subject;
    public Map<String, Object> content = new HashMap<String, Object>() ;
}
