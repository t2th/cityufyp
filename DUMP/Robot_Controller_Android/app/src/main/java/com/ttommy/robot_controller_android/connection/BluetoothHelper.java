package com.ttommy.robot_controller_android.connection;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.ttommy.robot_controller_android.SettingsActivity;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

/**
 * Created by TTommy on 10/2/2017.
 */

public class BluetoothHelper {
    private static BluetoothHelper Instance = null;
    public static BluetoothHelper getInstance(Context context){
        if(Instance == null) Instance = new BluetoothHelper(context);
        return Instance;
    }
    SharedPreferences sharedPref;
    String robotName  = "";

    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice = null;
    BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    UUID uuid = UUID.fromString("94f39d29-7d6d-437d-973b-fba39e49d4ee"); //Standard SerialPortService ID

    final byte delimiter = 33;
    int readBufferPosition = 0;
    ConnectedThread ct = null;
    private BluetoothHelper(Context context){
        sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        robotName = sharedPref.getString("robot_name", "");

        startConnection();
    }

    public void startConnection(){
        if(!mBluetoothAdapter.isEnabled())
        {
            Log.d("BluetoothHelper", "Bluetooth Adapter not enabled");
        }
        Log.d("BluetoothHelper", "Bluetooth Adapter enabled");

        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        if(pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                if (device.getName().equals(robotName))
                {
                    Log.d("BluetoothHelper", "Connected: "+device.getName());
                    mmDevice = device;
                    break;
                }
                Log.d("BluetoothHelper", device.getName());
            }
        }
        if(mmDevice!=null) {
            if(ct != null ){
                ct.cancel();
            }
            try {
                ct = new ConnectedThread(mmDevice.createRfcommSocketToServiceRecord(uuid));
                ct.start();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public void sendBtMsg(String msg2send){
        if(ct!=null){
            ct.write(msg2send.getBytes());
        }
    }
}
