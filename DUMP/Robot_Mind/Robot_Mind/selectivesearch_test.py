import cv2

cap = cv2.VideoCapture(0)
cv2.namedWindow("Input")
cv2.namedWindow("Output")


cap.set(cv2.CAP_PROP_FRAME_WIDTH,267)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT,200)

if __name__ == '__main__':
    # If image path and f/q is not passed as command
    # line arguments, quit and display help message
 
    # speed-up using multithreads
    cv2.setUseOptimized(True);
    cv2.setNumThreads(4);
 
    while True:
        # read image
        rect, im = cap.read()
        # resize image
        #newHeight = 200
        #newWidth = int(im.shape[1]*200/im.shape[0])
        #im = cv2.resize(im, (newWidth, newHeight))    
 
        # create Selective Search Segmentation Object using default parameters
        ss = cv2.ximgproc.segmentation.createSelectiveSearchSegmentation()
 
        # set input image on which we will run segmentation
        ss.setBaseImage(im)
 
        #ss.switchToSelectiveSearchFast()
        ss.switchToSingleStrategy()
        # run selective search segmentation on input image
        rects = ss.process()
        print('Total Number of Region Proposals: {}'.format(len(rects)))
     
        # number of region proposals to show
        numShowRects = 100
        # increment to increase/decrease total number
        # of reason proposals to be shown
        increment = 50
 
        # create a copy of original image
        imOut = im.copy()
 
        # itereate over all the region proposals
        for i, rect in enumerate(rects):
            # draw rectangle for region proposal till numShowRects
            if (i < numShowRects):
                x, y, w, h = rect
                cv2.rectangle(imOut, (x, y), (x+w, y+h), (0, 255, 0), 1, cv2.LINE_AA)
            else:
                break
        # show output
        cv2.imshow("Input", im)
        cv2.imshow("Output", imOut)
        key = cv2.waitKey(1) & 0xFF

    # close image show window
    cv2.destroyAllWindows()
