class CommandShelf:
    DefaultCommand = {
        "check" : object
        }
    
    def __init__(self, core):
        self.core = core
        self.BodyCommand = {}
        pass
    
    def get_commands(self):
        return {**DefaultCommand, **self.BodyCommand}