import threading
import time
from bluetooth import *

uuid = "94f39d29-7d6d-437d-973b-fba39e49d4ee"
server_name = "TTOMMY-PC"

class _BluetoothThread(threading.Thread):
    def __init__(self, core):
        super().__init__()
        self.core =core
        self.threadID = 1
        self.name = "bluetooth"
    def run(self):
        server_sock=BluetoothSocket( RFCOMM )
        server_sock.bind(("",PORT_ANY))
        server_sock.listen(1)

        port = server_sock.getsockname()[1]

        advertise_service( server_sock, server_name,
                           service_id = uuid,
                           service_classes = [ uuid, SERIAL_PORT_CLASS ],
                           profiles = [ SERIAL_PORT_PROFILE ], 
#                          protocols = [ OBEX_UUID ] 
        )
        print ("Waiting for connection on RFCOMM channel %d" % port)
        client_sock, client_info = server_sock.accept()
        while True:
            print ("Accepted connection from ", client_info)
            try:
                data = client_sock.recv(1024)
                if len(data) == 0: break
                #print(data)
                response = self.core.handle_request(data)
                client_sock.send(response)

            except IOError:
                pass

class BluetoothHandler:
    def __init__(self, core):
        self.core = core
        self._thread = _BluetoothThread(core)
        self._thread.start()