import numpy as np
import cv2
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile
from pathlib import Path

from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image

# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")

from robot_mind.object_detection.utils import label_map_util as label_map_util

from robot_mind.object_detection.utils import visualization_utils as vis_util
os.chdir("D:/cityufyp/Robot_Mind/Robot_Mind/robot_mind/object_detection")
print("Model preparation");
# What model to download.
MODEL_NAME = 'ssd_mobilenet_v1_coco_2017_11_17'
MODEL_FILE = MODEL_NAME + '.tar.gz'

DOWNLOAD_BASE = 'http://download.tensorflow.org/models/object_detection/'

# Path to frozen detection graph. This is the actual model that is used for the object detection.
PATH_TO_CKPT = MODEL_NAME + '/frozen_inference_graph.pb'

# List of the strings that is used to add correct label for each box.
PATH_TO_LABELS = os.path.join('data', 'mscoco_label_map.pbtxt')

NUM_CLASSES = 90

print("Download Model");
my_file = Path("D:/cityufyp/Robot_Mind/Robot_Mind/robot_mind/object_detection/"+MODEL_FILE)
if not my_file.is_file():
  opener = urllib.request.URLopener()
  opener.retrieve(DOWNLOAD_BASE + MODEL_FILE, MODEL_FILE)
tar_file = tarfile.open(MODEL_FILE)
for file in tar_file.getmembers():
  file_name = os.path.basename(file.name)
  if 'frozen_inference_graph.pb' in file_name:
    tar_file.extract(file, os.getcwd())
    detection_graph = tf.Graph()

print("Load a (frozen) Tensorflow model into memory.");
with detection_graph.as_default():
  od_graph_def = tf.GraphDef()
  with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
    serialized_graph = fid.read()
    od_graph_def.ParseFromString(serialized_graph)
    tf.import_graph_def(od_graph_def, name='')

label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)

def load_image_into_numpy_array(image):
  (im_width, im_height) = image.size
  return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8)
# initialize the list of reference points and boolean indicating
# whether cropping is being performed or not
refPt = []
cropping = False
latestImage = 0
ix, iy = -1, -1

def click_and_crop(event, x, y, flags, param):
    # grab references to the global variables
    global refPt, cropping, currentPt
 
    # if the left mouse button was clicked, record the starting
    # (x, y) coordinates and indicate that cropping is being
    # performed
    if event == cv2.EVENT_LBUTTONDOWN:
        refPt = [(x, y)]
        cropping = True

    elif event ==cv2.EVENT_MOUSEMOVE:
        currentPt = [(x,y)]

    # check to see if the left mouse button was released
    elif event == cv2.EVENT_LBUTTONUP:
        # record the ending (x, y) coordinates and indicate that
        # the cropping operation is finished
        refPt.append((x, y))
        cropping = False

        roi = latestImage[
            min(refPt[0][1], refPt[1][1]):max(refPt[0][1], refPt[1][1]),
            min(refPt[0][0], refPt[1][0]):max(refPt[0][0], refPt[1][0])]
        
        height, width = roi.shape[:2]
        if width>0 and height>0:
            cv2.imshow("ROI", roi)

cap = cv2.VideoCapture(0)
cv2.namedWindow("frame")

cv2.setMouseCallback("frame", click_and_crop)

cap.set(cv2.CAP_PROP_FRAME_WIDTH,352)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT,240)

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
session = tf.Session(config=config)
config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.7
session = tf.Session(config=config)
sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))

with tf.device('/device:GPU:0'):
  with detection_graph.as_default():
    with tf.Session(graph=detection_graph) as sess:
      # Definite input and output Tensors for detection_graph
      image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
      # Each box represents a part of the image where a particular object was detected.
      detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
      # Each score represent how level of confidence for each of the objects.
      # Score is shown on the result image, together with the class label.
      detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
      detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
      num_detections = detection_graph.get_tensor_by_name('num_detections:0')
      while(True):
          if(not cropping):
              # Capture frame-by-frame
              ret, frame = cap.read()
  
              # Our operations on the frame come here
              #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
              latestImage = frame;
              # Display the resulting frame
              cv2.imshow('frame',frame)
                    # the array based representation of the image will be used later in order to prepare the
              # result image with boxes and labels on it.
              image_np = load_image_into_numpy_array( Image.fromarray(frame))
              # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
              image_np_expanded = np.expand_dims(image_np, axis=0)
              # Actual detection.
              (boxes, scores, classes, num) = sess.run(
                [detection_boxes, detection_scores, detection_classes, num_detections],
                feed_dict={image_tensor: image_np_expanded})
              vis_util.visualize_boxes_and_labels_on_image_array(
                image_np,
                np.squeeze(boxes),
                np.squeeze(classes).astype(np.int32),
                np.squeeze(scores),
                category_index,
                use_normalized_coordinates=True,
                line_thickness=8)
              cv2.imshow("result", image_np)
          else:
              tmp = latestImage.copy();
              cv2.rectangle(tmp, refPt[0], currentPt[0], (0, 255, 0), 2)
              cv2.imshow("frame", tmp)
  
          key = cv2.waitKey(1) & 0xFF


# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
