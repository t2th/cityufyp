import sys
from ctrl_handler.bluetooth_handler import BluetoothHandler
from command.command_shelf import CommandShelf
from body_driver.body_shelf import BodyShelf
import json
from collections import namedtuple

class RobotCore:
    def __init__(self):
        self.controller = BluetoothHandler(self)
        self.command_shelf = CommandShelf(self)
        self.body_shelf = BodyShelf(self)

    def handle_request(self, data):
        try:
            object = json.loads(data, object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))
            print(object)
            return json.dumps({"test": "OK"}, default=lambda o: o.__dict__)
        except json.JSONDecodeError:
            print("decode error")

if __name__ == "__main__":
    try:
        CORE = RobotCore()
        input("Press Enter to continue...")
    except KeyboardInterrupt:
        pass