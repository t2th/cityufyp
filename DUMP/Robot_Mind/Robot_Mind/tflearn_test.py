from __future__ import division, print_function, absolute_import

import tflearn
from tflearn.layers.core import input_data, dropout, fully_connected
from tflearn.layers.conv import conv_2d, max_pool_2d
from tflearn.layers.normalization import local_response_normalization
from tflearn.layers.estimator import regression

# Data loading and preprocessing
import tflearn.datasets.mnist as mnist
X, Y, testX, testY = mnist.load_data(one_hot=True)
X = X.reshape([-1, 28, 28, 1])
testX = testX.reshape([-1, 28, 28, 1])

# Building convolutional network
network = input_data(shape=[None, 28, 28, 1], name='input')
network = conv_2d(network, 32, 3, activation='relu', regularizer="L2")
network = max_pool_2d(network, 2)
network = local_response_normalization(network)
network = conv_2d(network, 64, 3, activation='relu', regularizer="L2")
network = max_pool_2d(network, 2)
network = local_response_normalization(network)
network = fully_connected(network, 128, activation='tanh')
network = dropout(network, 0.8)
network = fully_connected(network, 256, activation='tanh')
network = dropout(network, 0.8)
network = fully_connected(network, 10, activation='softmax')
network = regression(network, optimizer='adam', learning_rate=0.01,
                     loss='categorical_crossentropy', name='target')

# initialize the list of reference points and boolean indicating
# whether cropping is being performed or not
refPt = []
cropping = False
latestImage = 0
ix, iy = -1, -1

def click_and_crop(event, x, y, flags, param):
    # grab references to the global variables
    global refPt, cropping, currentPt
 
    # if the left mouse button was clicked, record the starting
    # (x, y) coordinates and indicate that cropping is being
    # performed
    if event == cv2.EVENT_LBUTTONDOWN:
        refPt = [(x, y)]
        cropping = True

    elif event ==cv2.EVENT_MOUSEMOVE:
        currentPt = [(x,y)]

    # check to see if the left mouse button was released
    elif event == cv2.EVENT_LBUTTONUP:
        # record the ending (x, y) coordinates and indicate that
        # the cropping operation is finished
        refPt.append((x, y))
        cropping = False

        roi = latestImage[
            min(refPt[0][1], refPt[1][1]):max(refPt[0][1], refPt[1][1]),
            min(refPt[0][0], refPt[1][0]):max(refPt[0][0], refPt[1][0])]
        
        height, width = roi.shape[:2]
        if width>0 and height>0:
            cv2.imshow("ROI", roi)

cap = cv2.VideoCapture(0)
cv2.namedWindow("frame")

cv2.setMouseCallback("frame", click_and_crop)

cap.set(cv2.CAP_PROP_FRAME_WIDTH,352);
cap.set(cv2.CAP_PROP_FRAME_HEIGHT,240);

while(True):
        if(not cropping):
            # Capture frame-by-frame
            ret, frame = cap.read()

            # Our operations on the frame come here
            #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            latestImage = frame;
            # Display the resulting frame
            cv2.imshow('frame',frame)
                  # the array based representation of the image will be used later in order to prepare the
            # result image with boxes and labels on it.
            image_np = load_image_into_numpy_array( Image.fromarray(frame))
            # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
            image_np_expanded = np.expand_dims(image_np, axis=0)
            # Actual detection.
            (boxes, scores, classes, num) = sess.run(
              [detection_boxes, detection_scores, detection_classes, num_detections],
              feed_dict={image_tensor: image_np_expanded})
            vis_util.visualize_boxes_and_labels_on_image_array(
              image_np,
              np.squeeze(boxes),
              np.squeeze(classes).astype(np.int32),
              np.squeeze(scores),
              category_index,
              use_normalized_coordinates=True,
              line_thickness=8)
            cv2.imshow("result", image_np)
        else:
            tmp = latestImage.copy();
            cv2.rectangle(tmp, refPt[0], currentPt[0], (0, 255, 0), 2)
            cv2.imshow("frame", tmp)

        key = cv2.waitKey(1) & 0xFF