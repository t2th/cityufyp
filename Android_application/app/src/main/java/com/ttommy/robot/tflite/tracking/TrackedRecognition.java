package com.ttommy.robot.tflite.tracking;


import android.graphics.RectF;

public class TrackedRecognition {
    public ObjectTracker.TrackedObject getTrackedObject() {
        return trackedObject;
    }

    public RectF getLocation() {
        return (location == null)
                ? trackedObject.getTrackedPositionInPreviewFrame()
                : new RectF(location);
    }

    public float getDetectionConfidence() {
        return detectionConfidence;
    }

    public int getColor() {
        return color;
    }

    public String getTitle() {
        return title;
    }

    ObjectTracker.TrackedObject trackedObject;
    RectF location;
    float detectionConfidence;
    int color;
    String title;
}