package com.ttommy.robot.service

import android.util.Log
import com.ttommy.robot.model.ArMarker
import com.ttommy.robot.model.ObjectDetectResult
import com.ttommy.robot.tflite.tracking.TrackedRecognition
import io.socket.client.IO
import io.socket.client.Socket
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by TTommy on 3/11/2018.
 */
class MainServerService{
    val TAG  = this.javaClass.name
    private object Holder { val INSTANCE = MainServerService() }
    var isConnected = false
    var url:String? = "192.168.1.100:6801"
    var mac:String? = "01-00-00-00-00-00"
    var lastDetectResult: ArrayList<ObjectDetectResult> = ArrayList()

    private var socket: Socket? = null


    fun getAvailDevice(): HashMap<String, String>{
        var result =  HashMap<String, String>()
        return result
    }

    fun start(url: String? = this.url){
        this.url = url
        val opts = IO.Options()
        //opts.forceNew = true;
        //opts.reconnection = true;

        if(isConnected)
            return
        Log.d(TAG, "try connect to $url")
        socket = initSocket(opts)
        socket?.connect()
        Log.i(TAG, "started  main server  service")
    }

    private fun initSocket(opts: IO.Options): Socket {
        var socket = IO.socket("http://$url/robot", opts)
        socket?.on(Socket.EVENT_CONNECT, {
            this.isConnected = true
            socket?.emit("joined")
            Log.i(TAG, "connected to $url")
        })
        socket?.on("oupdate_ar_position",{
            Log.d(TAG, "oupdate_ar_position")
        })
        socket?.on("update_in_sight", {
            Log.d(TAG, "update_in_sight")
            Log.d(TAG, it[0].toString())
            val obj = it[0] as JSONObject
            var resultArr = obj.getJSONArray("results")
            lastDetectResult = ArrayList()
            for(i in 0 until resultArr.length()){
                lastDetectResult.add(ObjectDetectResult(resultArr.getJSONObject(i)))
            }
        })
        socket?.on(Socket.EVENT_DISCONNECT,  {
            this.isConnected = false
            Log.e(TAG, "disconnected")
        })
        return socket
    }

    fun stop(){
        if(!isConnected)
            return
        socket?.emit("left")
        socket?.disconnect()
        Log.i(TAG, "stopped main server service")
    }
    fun getDid(){

    }
    fun reportArMarker(markers: List<ArMarker>){
        if(isConnected){

            var tmp:JSONArray = JSONArray()
            markers.forEach(){
                tmp.put(it.toJSON())
            }
            var result = JSONObject()
            result.put("did", this.mac)
            result.put("markers", tmp)
            socket?.emit("feed_ar", result)
        }
        else
            Log.w(TAG, "main server socket io disconnected")
    }

    fun reportDetectedObject(mappedObj: List<TrackedRecognition> ){

        if(isConnected){
            var tmp:JSONArray = JSONArray()
            mappedObj.forEach(){
                var jo = JSONObject()
                jo.put("name", it.title)
                jo.put("score", it.detectionConfidence)
                var ja = JSONArray()
                ja.put(it.location.top/480)
                ja.put(it.location.left/640)
                ja.put(it.location.bottom/480)
                ja.put(it.location.right/640)
                jo.put("mobileId", it.trackedObject.id)
                jo.put("box", ja)
                tmp.put(jo)
            }
            var result = JSONObject()
            result.put("did", this.mac)
            result.put("results", tmp)
            socket?.emit("feed_objects", result)
        }
        else
            Log.w(TAG, "main server socket io disconnected")

    }
    fun getDetectionByMobileId(mid: String): ObjectDetectResult?{
        return lastDetectResult.find { it.mobileId==mid }
    }
    companion object {
        val instance: MainServerService by lazy { Holder.INSTANCE }
    }
}