package com.ttommy.robot.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ttommy.robot.R
import com.ttommy.robot.service.RobotService
import kotlinx.android.synthetic.main.fragment_robot_status.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import org.json.JSONObject
import java.net.URL

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [RobotStatusFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class RobotStatusFragment : Fragment() {

    var isLoop= false
    var updateThread:Thread? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnDiscover.setOnClickListener(){
            RobotService.instance.performDiscover()
        }

        btnStopAll.setOnClickListener(){
            RobotService.instance.performStop()
        }
    }

    override fun onResume() {
        super.onResume()
        RobotService.instance.start()
        updateThread = Thread{
            updateLoop()
        }
        isLoop= true
        updateThread?.start()
    }

    override fun onPause() {
        super.onPause()
        RobotService.instance.stop()
        isLoop = false
    }

    private fun updateLoop() {

        while(isLoop){
            try {
                val url = URL("http://${RobotService.instance.url}/get_action").readText()
                var jObj = JSONObject(url);
                var actionArr = jObj.getJSONArray("actions")
                var actionList = ArrayList<String>()
                for(i in 0 until actionArr.length()){
                    actionList.add( actionArr.getString(i))
                }
                if(!isLoop)
                    break
                launch(UI){
                    tvActionList.text = actionList?.toString()
                    if(RobotService.instance.isConnected)
                        tvRobotStatus.text = "Connected"
                    else
                        tvRobotStatus.text = "Disconnected"
                    tvUrl.text = RobotService.instance.url?.toString()
                }
            }catch (e: Exception){
                e.printStackTrace()
            }
            Thread.sleep(1000)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_robot_status, container, false)
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment RobotStatusFragment.
         */
        @JvmStatic
        fun newInstance() =
                RobotStatusFragment().apply {
                    arguments = Bundle().apply {
                    }
                }
    }
}
