package com.ttommy.robot.service

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import com.ttommy.robot.model.ObjectDetectResult
import io.socket.client.IO
import io.socket.client.Socket
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL


/**
 * Created by TTommy on 3/11/2018.
 */
class RobotService{
    val TAG  = this.javaClass.name
    val RECONNECT_INTERVAL: Long =1000
    val GET_FRAME_INTERVAL: Long = 80

    private object Holder { val INSTANCE = RobotService() }
    var isStop= false
    var isConnected = false
    var isGetFrame = false

    var url:String? = null
    get() = field
    set(value) {field = value}

    var connectionThread:Thread? = null
    var updateFrameThread:Thread? = null
    var lastFrame: Bitmap? = null
    var lastDetectionRecord: MutableList<ObjectDetectResult> = ArrayList()

    private var socket: Socket? = null

    fun start(url: String? = this.url){

        if(isConnected)
            return
        isStop = false
        this.url = url
//        connectionThread = Thread{
//            connectionCheckLoop()
//        }
//        connectionThread?.start()
        val opts = IO.Options()

        //opts.forceNew = true;
        //opts.reconnection = true;
        Log.d(TAG, "try connect to $url")
        socket = initSocket(opts)
        socket?.connect()
        Log.i(TAG, "started robot service")
    }
    private fun initSocket(opts: IO.Options): Socket {
        var socket = IO.socket("http://$url", opts)
        socket?.on(Socket.EVENT_CONNECT, {
            this.isConnected = true
            socket?.emit("joined")
            Log.i(TAG, "connected to $url")
        })
        socket?.on("status",{
            Log.d(TAG, "status")
            val obj = it[0] as JSONObject
            Log.d(TAG, obj.toString())
        })
        socket?.on("detectedObject", {
            Log.d(TAG, "detectedObject")
            val obj = it[0] as JSONObject
            onDetectObjectReceived(obj)
        })
        socket?.on(Socket.EVENT_DISCONNECT,  {
            this.isConnected = false
            Log.e(TAG, "disconnected")
        })
        return socket
    }

    private fun onDetectObjectReceived(objects: JSONObject) {
        lastDetectionRecord = ArrayList()
        var arr = objects.getJSONArray("results")
        for(i in 0 .. arr.length()-1){
            lastDetectionRecord.add(ObjectDetectResult(arr.getJSONObject(i)))
        }
    }

    fun startGetFrame(){
        isGetFrame = true
        updateFrameThread = Thread{
            updateLatestFrameLoop()
        }
        updateFrameThread?.start()
        Log.i(TAG, "started robot service: get frame")
    }
//
//    private fun connectionCheckLoop() {
//        while(!isStop){
//            try {
//                Log.d(TAG, "try connect to $url")
//                val result = URL("http://$url/ping").readText()
//                isConnected = true
//                Log.i(TAG, "connected to $url")
//            }catch (e: Exception){
//                e.printStackTrace()
//                Log.e(TAG, "connect fail")
//                isConnected = false
//            }
//            Thread.sleep(RECONNECT_INTERVAL)
//        }
//    }
    private fun updateLatestFrameLoop(){
        Log.d(TAG, "updateLatestFrameLoop")
        while(isGetFrame){
            try {
                val url = URL("http://$url/capture")
                val connection = url.openConnection() as HttpURLConnection
                connection.doInput = true
                connection.connect()
                val input = connection.inputStream
                lastFrame = BitmapFactory.decodeStream(input)
            }catch (e: Exception){
                e.printStackTrace()
            }
            Thread.sleep(GET_FRAME_INTERVAL)
        }
    }

    fun stopGetFrame(){
        isGetFrame = false
        Log.i(TAG, "stopped robot service: get frame")
    }
    fun stop(){
        if(!isConnected)
            return
        isStop = true
        socket?.emit("left")
        socket?.disconnect()
        Log.i(TAG, "stopped robot service")
    }
    fun performLookAt(oid:String){

        if(isConnected){

            var request = JSONObject()
            request.put("action", "lookat")
            request.put("oid", oid)
            socket?.emit("action", request)
        }
        else
            Log.w(TAG, "robot server socket io disconnected")
    }
    fun performStop(){

        if(isConnected){

            var request = JSONObject()
            request.put("action", "stopaction")
            socket?.emit("action", request)
        }
        else
            Log.w(TAG, "robot server socket io disconnected")
    }
    fun performDiscover(){

        if(isConnected){

            var request = JSONObject()
            request.put("action", "discover")
            socket?.emit("action", request)
        }
        else
            Log.w(TAG, "robot server socket io disconnected")
    }
    fun performGoto(oid:String){

        if(isConnected){

            var request = JSONObject()
            request.put("action", "goto")
            request.put("oid", oid)
            socket?.emit("action", request)
        }
        else
            Log.w(TAG, "robot server socket io disconnected")
    }
/*    fun performAction(actionType: String, vararg args:JsonObject){
        var request = JSONObject()
        request.put("action", actionType)
        request.put("")
    }*/
    companion object {
        val instance: RobotService by lazy { Holder.INSTANCE }
    }
}
