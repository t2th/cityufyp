package com.ttommy.robot.model

import org.json.JSONObject

class ObjectDetectResult{
    var box: DoubleArray = DoubleArray(4)
    var name: String = ""
    var oid: String = ""
    var pos: DoubleArray = DoubleArray(3)
    var score: Double = 0.0
    var mobileId: String = ""
    constructor(){

    }
    constructor(json: JSONObject){
        name= json.getString("name")
        oid = json.getString("oid")
        score = json.getDouble("score")
        var arrayBox = json.getJSONArray("box")
        for(i in 0 until arrayBox.length()){
            box[i] = arrayBox.getDouble(i)
        }
        var arrayPos = json.getJSONArray("pos")
        for(i in 0 until arrayPos.length()){
            pos[i] = arrayPos.getDouble(i)
        }
        if(json.has("mobileId"))
            mobileId = json.getString("mobileId")
    }
}