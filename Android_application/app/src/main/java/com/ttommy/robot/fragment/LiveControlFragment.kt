package com.ttommy.robot.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.ttommy.robot.R
import com.ttommy.robot.service.RobotService
import com.ttommy.robot.view.LiveControlView
import kotlinx.android.synthetic.main.fragment_live_control.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [LiveControlFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [LiveControlFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class LiveControlFragment : Fragment() {
    val TAG = "LiveControlFragment"

    private var liveView: LiveControlView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_live_control, container, false)
    }

    override fun onResume() {
        super.onResume()
        RobotService.instance.startGetFrame()

        RobotService.instance.start()

    }

    override fun onPause() {
        super.onPause()
        RobotService.instance.stopGetFrame()

        RobotService.instance.stop()
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        liveView= LiveControlView(context)
        linearLayout.addView(liveView)
        liveView?.setOnTouchListener{
            v, event ->
            onLiveViewTouch(v, event)
        }
    }

    private fun onLiveViewTouch(v: View, event: MotionEvent): Boolean {
        if(event.getAction() == MotionEvent.ACTION_DOWN) {
            Log.d(TAG, "DOWN: x: ${event.x}, y: ${event.y}")
            var rx = event.x / liveView!!.w
            var ry = event.y / liveView!!.h
            Log.d(TAG, "w=${liveView!!.w}, h=${liveView!!.h}")
            Log.d(TAG, "rx=$rx, ry=$ry")
            RobotService.instance.lastDetectionRecord.forEach {
                if (it.box[1] <= rx && rx <= it.box[3] && it.box[0] <= ry && ry <= it.box[2]) {
                    Log.d(TAG, "clicked on ${it.name}")
                    Toast.makeText(context, "you clicked on ${it.name}", Toast.LENGTH_SHORT).show()
                }
            }
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            Log.d(TAG, "UP: x: ${event.x}, y: ${event.y}")
        }
        return true
    }

    companion object {
        @JvmStatic
        fun newInstance() =
                LiveControlFragment().apply {
                    arguments = Bundle().apply {
                    }
                }
    }
}
