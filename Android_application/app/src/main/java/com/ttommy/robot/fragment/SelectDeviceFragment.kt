package com.ttommy.robot.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import com.ttommy.robot.R
import com.ttommy.robot.service.MainServerService
import com.ttommy.robot.service.RobotService
import kotlinx.android.synthetic.main.fragment_select_device.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import org.json.JSONObject
import java.net.URL


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class SelectDeviceFragment : Fragment() {
    //var pref = activity.getSharedPreferences("test", MODE_PRIVATE)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_select_device, container, false)
    }


    private var deviceMap: HashMap<String, String> = HashMap()

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getAvailDevice()

        btnRefresh.setOnClickListener(){
            getAvailDevice()
        }
        btnApply.setOnClickListener(){
            Log.d("ui", spinner.selectedItem.toString())
            RobotService.instance.stop()
            RobotService.instance.url = deviceMap[spinner.selectedItem.toString()]+":6802"
            Toast.makeText(activity, "connected to ${spinner.selectedItem} at ${ RobotService.instance.url }", Toast.LENGTH_LONG).show()
        }
    }

    private fun getAvailDevice(){
        var result =  HashMap<String, String>()
        async{
            var apiResponse = URL("http://${MainServerService.instance.url}/get_avail_device").readText()
            Log.d("MainServer", apiResponse)
            var jObj = JSONObject(apiResponse)
            var devices = jObj.getJSONArray("data")
            for (i in 0..(devices.length() - 1)) {
                val item = devices.getJSONObject(i)

                result.put(item.getString("did"), item.getString("ip"))
            }
            deviceMap = result
            launch(UI){
                val array_adapter = ArrayAdapter(activity, android.R.layout.simple_spinner_item, deviceMap.keys.toList())
                array_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinner!!.setAdapter(array_adapter)
            }
        }
    }


    companion object {
        @JvmStatic
        fun newInstance() =
                SelectDeviceFragment().apply {
                    arguments = Bundle().apply {
                    }
                }
    }
}
