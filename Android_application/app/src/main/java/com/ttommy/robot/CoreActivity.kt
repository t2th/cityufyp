package com.ttommy.robot

import android.content.Context
import android.content.Intent
import android.hardware.camera2.CameraAccessException
import android.hardware.camera2.CameraCharacteristics
import android.hardware.camera2.CameraManager
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.util.SizeF
import android.view.Menu
import android.view.MenuItem
import com.ttommy.robot.activity.ControllerActivity
import com.ttommy.robot.fragment.HomeFragment
import com.ttommy.robot.fragment.LiveControlFragment
import com.ttommy.robot.fragment.RobotStatusFragment
import com.ttommy.robot.fragment.SelectDeviceFragment
import com.ttommy.robot.opencv.CameraCalibrationActivity
import com.ttommy.robot.service.ArucoService
import com.ttommy.robot.service.MainServerService
import com.ttommy.robot.service.RobotService
import kotlinx.android.synthetic.main.activity_core.*
import kotlinx.android.synthetic.main.app_bar_core.*
import org.opencv.android.BaseLoaderCallback
import org.opencv.android.LoaderCallbackInterface
import org.opencv.android.OpenCVLoader
import java.net.NetworkInterface
import java.util.*




class CoreActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (!OpenCVLoader.initDebug()) {
            // Handle initialization error
        }
        setContentView(R.layout.activity_core)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
        Log.d("", "hi")

        var fragmentManager = supportFragmentManager
        fragmentManager
                .beginTransaction()
                .replace(R.id.myfragment, HomeFragment.newInstance() as Fragment)
                .commit()
    }

    private val mLoaderCallback = object : BaseLoaderCallback(this) {
        override fun onManagerConnected(status: Int) {
            when (status) {
                LoaderCallbackInterface.SUCCESS -> {
                    Log.i("OpenCV", "OpenCV loaded successfully")
                    ArucoService.instance.init(this@CoreActivity)
                }
                else -> {
                    super.onManagerConnected(status)
                }
            }
        }
    }

    private fun getCameraResolution(camNum: Int): SizeF? {
        var size: SizeF? = SizeF(0f, 0f)
        val manager = getSystemService(Context.CAMERA_SERVICE) as CameraManager
        try {
            val cameraIds = manager.cameraIdList
            if (cameraIds.size > camNum) {
                val character = manager.getCameraCharacteristics(cameraIds[camNum])
                size = character.get(CameraCharacteristics.SENSOR_INFO_PHYSICAL_SIZE)
            }
        } catch (e: CameraAccessException) {
            Log.e("YourLogString", e.message, e)
        }

        return size
    }
    override fun onResume() {
        super.onResume()
        if (!OpenCVLoader.initDebug()) {
            Log.d("OpenCV", "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_4_0, this, mLoaderCallback);
        } else {
            Log.d("OpenCV", "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
        Log.d("camera", ""+getCameraResolution(0));
    }

    fun getMacAddr(): String {
        try {
            val all = Collections.list(NetworkInterface.getNetworkInterfaces())
            for (nif in all) {
                if (nif.getName().toLowerCase()!="wlan0") continue

                val macBytes = nif.getHardwareAddress() ?: return ""

                val res1 = StringBuilder()
                for (b in macBytes) {
                    res1.append(String.format("%02X:", b))
                }

                if (res1.length > 0) {
                    res1.deleteCharAt(res1.length - 1)
                }
                return res1.toString()
            }
        } catch (ex: Exception) {
        }

        return "02:00:00:00:00:00"
    }

    override fun onStart() {
        super.onStart()

        RobotService.instance.start()
        MainServerService.instance.start()

        MainServerService.instance.mac = getMacAddr()
        Log.d("Core", "Mac Address: "+MainServerService.instance.mac)
    }
    override fun onStop(){
        super.onStop()
    }

    override fun onDestroy() {

//        RobotService.instance.stop()
//        MainServerService.instance.stop()
        super.onDestroy()

    }

    override fun onPause() {
        super.onPause()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.core, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        /*if (item.itemId == R.id.nav_tflite_test1 ){
            val intent = Intent(this, ControllerActivity::class.java)
            startActivity(intent)
            return false
        }*/
        if( item.itemId == R.id.nav_calibrate){
            val intent = Intent(this, CameraCalibrationActivity::class.java)
            startActivity(intent)
            return false
        }else if (  item.itemId == R.id.nav_control ){
            val intent = Intent(this, ControllerActivity::class.java)
            startActivity(intent)
            return false
        }
        // Handle navigation view item clicks here.
        var fragment: Fragment = when (item.itemId) {
            R.id.nav_home -> HomeFragment.newInstance()
            R.id.nav_start_connection -> SelectDeviceFragment.newInstance()
            R.id.nav_live_control -> LiveControlFragment.newInstance()
            R.id.nav_robot_status -> RobotStatusFragment.newInstance()
            //R.id.nav_setting -> HomeFragment.newInstance() //TODO:
            //R.id.nav_direct_control -> DirectControlFragment.newInstance()
            else -> HomeFragment.newInstance()
        }
        var fragmentManager = supportFragmentManager
        fragmentManager
                .beginTransaction()
                .replace(R.id.myfragment, fragment)
                .commit()
        item.isChecked = true

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
