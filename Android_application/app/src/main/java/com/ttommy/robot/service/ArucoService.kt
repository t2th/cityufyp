package com.ttommy.robot.service

import android.app.Activity
import android.graphics.Bitmap
import android.util.Log
import com.ttommy.robot.model.ArMarker
import com.ttommy.robot.opencv.CalibrationResult
import org.opencv.android.Utils
import org.opencv.aruco.Aruco
import org.opencv.aruco.DetectorParameters
import org.opencv.aruco.Dictionary
import org.opencv.calib3d.Calib3d
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.core.Size
import org.opencv.imgproc.Imgproc


class ArucoService{
    val TAG ="Aruco detect marker"
    private object Holder { val INSTANCE = ArucoService() }
    var mapx:Mat? = Mat()
    var mapy: Mat? = Mat()
    var arucoDict:Dictionary? = null
    var arucoParams: DetectorParameters?=null
    var cameraMat = Mat()
    var distortionCoefficients = Mat()
    val w =640.00
    val h = 480.00
    val markerLength = 10
    var inited = false

    fun init(activity: Activity){
        if(inited)
            return
        try{
            Mat.eye(3, 3, CvType.CV_64FC1).copyTo(cameraMat)
            cameraMat.put(0, 0, 1.0)
            Mat.zeros(5, 1, CvType.CV_64FC1).copyTo(distortionCoefficients)
            CalibrationResult.tryLoad(activity, cameraMat, distortionCoefficients)

            arucoDict = Aruco.getPredefinedDictionary(Aruco.DICT_6X6_1000)
            arucoParams = DetectorParameters.create()
            Log.d("Aruco detect marker", "cameraMat: ${cameraMat.dump()}")
            Log.d("Aruco detect marker", "distortionCoefficients: ${distortionCoefficients.dump()}")
            var newCameraMatrix = Calib3d.getOptimalNewCameraMatrix(cameraMat, distortionCoefficients, Size(w, h), 1.00)
            Imgproc.initUndistortRectifyMap(cameraMat, distortionCoefficients, Mat(), newCameraMatrix, Size(w,h), CvType.CV_16SC2, mapx, mapy)
            inited = true
        }catch (e: Exception){
            Log.e("Aruco detect marker", "exception", e)
        }
    }

    fun detectArucoMarkerAndFeed(bitmap: Bitmap){
        Log.d("Aruco detect marker", "W: ${bitmap.width}H: ${bitmap.height}")
        val mat = Mat()
        val remap = Mat()
        val graymat = Mat()
        Utils.bitmapToMat(bitmap, mat)
        Imgproc.remap(mat, remap, mapx, mapy, Imgproc.INTER_LINEAR)
        Imgproc.cvtColor(remap, graymat, Imgproc.COLOR_BGR2GRAY)
        var corners:List<Mat> = ArrayList<Mat>()
        var ids:Mat = Mat()
        Aruco.detectMarkers(graymat, arucoDict,corners, ids)
        var rvecs:Mat = Mat()
        var tvecs:Mat = Mat()
        Aruco.estimatePoseSingleMarkers(corners, markerLength.toFloat(), cameraMat, distortionCoefficients, rvecs, tvecs)
        Log.d("Aruco detect marker", "result: ${ids?.dump()}")
        Log.d("Aruco detect marker", "rvecs: ${rvecs?.dump()}")
        Log.d("Aruco detect marker", "tvec: ${tvecs?.dump()}")
        val tempId = IntArray(ids.size().width.toInt() * ids.size().height.toInt())
        if(tempId.size>0)
            ids.get(0, 0, tempId)
        val tempRevc = DoubleArray(rvecs.total().toInt() * rvecs.elemSize().toInt() )
        if(tempRevc.size>0)
            rvecs.get(0, 0, tempRevc)
        val tempTevc = DoubleArray(tvecs.total().toInt()  * tvecs.elemSize().toInt())
        if(tempTevc.size>0)
            tvecs.get(0, 0, tempTevc)
        Log.d("Aruco detect marker", "size: ${tempId.size} ${tempRevc.size} ${tempTevc.size}")
        var  result = ArrayList<ArMarker>()
        if(tempId.size>0) {
            for (i in 0..tempId.size - 1) {
                var j = i * 3
                var ar = ArMarker()
                ar.id = tempId[i]
                ar.rvec[0] = tempRevc[j++]
                ar.rvec[1] = tempRevc[j++]
                ar.rvec[2] = tempRevc[j++]
                j = i * 3
                ar.tvec[0] = tempTevc[j++]
                ar.tvec[1] = tempTevc[j++]
                ar.tvec[2] = tempTevc[j++]
                result.add(ar)
            }
            MainServerService.instance.reportArMarker(result)
        }
    }


    companion object {
        val instance: ArucoService by lazy { Holder.INSTANCE }
    }
}