package com.ttommy.robot.activity

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Point
import android.graphics.RectF
import android.util.Log
import android.util.Size
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import com.ttommy.robot.service.MainServerService
import com.ttommy.robot.service.RobotService
import com.ttommy.robot.tflite.DetectorActivity





class ControllerActivity : DetectorActivity(){
    val TAG = "Controller Activity"
    val MAX_SKIP_FRAME = 240;
    var w = 1356
    var h = 965
    var isTouch = false
    var targetMid:String? = null
    var targetOid: String? = null
    var sourcePoint: Point? = null
    var lastPoint: Point? = null
    var action = -1;
    var skippedFrame:Int= 0;

    override fun onResume() {
        super.onResume()
        MainServerService.instance.start()
        RobotService.instance.start()
    }

    override fun onPause() {
        super.onPause()
        MainServerService.instance.stop()
        RobotService.instance.stop()
    }
    override fun onPreviewSizeChosen(size: Size?, rotation: Int) {


        super.onPreviewSizeChosen(size, rotation)
        (trackingOverlay as View).setOnTouchListener(
                object : View.OnTouchListener {
                    override fun onTouch(v: View, event: MotionEvent): Boolean {
                        if (event.getAction() == MotionEvent.ACTION_DOWN) {
                            Log.d(TAG, "DOWN: x: ${event.x}, y: ${event.y}")
                            var rx = event.x / w
                            var ry = event.y / h
                            Log.d(TAG, "w=${w}, h=${h}")
                            Log.d(TAG, "rx=$rx, ry=$ry")
                            MainServerService.instance.lastDetectResult.forEach {
                                if (it.box[1] <= rx && rx <= it.box[3] && it.box[0] <= ry && ry <= it.box[2]) {
                                    Log.d(TAG, "clicked on ${it.name}")
                                    //Toast.makeText(this@ControllerActivity, "you clicked on ${it.name}", Toast.LENGTH_SHORT).show()
                                    sourcePoint = Point(event.x.toInt(), event.y.toInt())
                                    lastPoint = sourcePoint
                                    targetMid = it.mobileId
                                    targetOid = it.oid
                                    isTouch = true
                                }
                            }
                        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
                            lastPoint = Point(event.x.toInt(), event.y.toInt())
                        } else if (event.getAction() == MotionEvent.ACTION_UP) {
                            if(isTouch) {
                                Log.d(TAG, "UP: x: ${event.x}, y: ${event.y}")
                                isTouch = false
                                if (action == 0) {
                                    var target = MainServerService.instance.lastDetectResult.find { it.mobileId == targetMid }
                                    if (target != null) {
                                        if (target.oid.isNullOrEmpty()) {
                                            Toast.makeText(this@ControllerActivity, "This object is not identified", Toast.LENGTH_SHORT).show()
                                        } else {
                                            RobotService.instance.performGoto(target.oid)
                                        }
                                    }
                                } else if (action == 1) {
                                    var target = MainServerService.instance.lastDetectResult.find { it.mobileId == targetMid }
                                    if (target != null) {
                                        if (target.oid.isNullOrEmpty()) {
                                            Toast.makeText(this@ControllerActivity, "This object is not identified", Toast.LENGTH_SHORT).show()
                                        } else {
                                            RobotService.instance.performLookAt(target.oid)
                                        }
                                    }
                                }
                            }
                            skippedFrame = 0
                            targetOid = null
                            targetMid = null
                            action = -1
                        }
                        return true
                    }
                })
    }

    override fun drawCanvas(canvas: Canvas){
        super.drawCanvas(canvas)
        if(isTouch) {
            var drawed = false
            val item = tracker.trackedObjects.find { it.trackedObject.id == targetMid }
            if(item != null){
                var rect = RectF(item.location)
                tracker.frameToCanvasMatrix.mapRect(rect)
                val whitePaint = Paint()
                whitePaint.style = Paint.Style.STROKE
                whitePaint.setARGB(180, 255, 255, 255)
                val blackPaint = Paint()
                blackPaint.style = Paint.Style.STROKE
                blackPaint.setARGB(230, 50, 50, 50)

                var radius = Math.min(Math.max(rect.width(), rect.height()) / 2f, h/3f)

                whitePaint.strokeWidth = (radius * 0.25).toFloat()
                blackPaint.strokeWidth = (radius * 0.25).toFloat()

                var cx = rect.centerX()
                var cy = rect.centerY()
                val textPaint = Paint()
                textPaint.setARGB(200, 0,0,0)
                textPaint.textAlign = Paint.Align.CENTER
                textPaint.textSize = radius/6
                val textBorder = Paint()
                textBorder.setARGB(200, 255,255,255)
                textBorder.strokeWidth = 5f
                textBorder.style = Paint.Style.STROKE
                textBorder.textAlign = Paint.Align.CENTER
                textBorder.textSize = radius/6

                val centerPoint = Point(cx.toInt(),cy.toInt())
                canvas.drawCircle(cx, cy, radius, whitePaint)
                if(lastPoint!!.y-centerPoint!!.y> radius/2){
                    canvas.drawArc(RectF(cx-radius, cy-radius, cx+radius, cy+radius), 0f, 180f, false, blackPaint);
//                        var arrOfColor = intArrayOf(Color.WHITE, Color.GRAY)
//                        var gradient = LinearGradient(cx, cy-radius, cx, cy+radius, arrOfColor, floatArrayOf(0F, 1F), Shader.TileMode.CLAMP)
//                        whitePaint.shader = gradient
                    action = 0
                }
                else if(centerPoint!!.y-lastPoint!!.y> radius/2){
                    canvas.drawArc(RectF(cx-radius, cy-radius, cx+radius, cy+radius), 180f, 180f, false, blackPaint);
//                        var arrOfColor = intArrayOf(Color.GRAY, Color.WHITE)
//                        var gradient = LinearGradient(cx, cy-radius, cx, cy+radius, arrOfColor, floatArrayOf(0F, 1F), Shader.TileMode.CLAMP)
//                        whitePaint.shader = gradient
                    action = 1
                }else{
                    action=-1
                }
                canvas.drawText("look", cx, cy -radius, textBorder)
                canvas.drawText("go to", cx, cy +radius, textBorder)
                canvas.drawText("look", cx, cy -radius, textPaint)
                canvas.drawText("go to", cx, cy +radius, textPaint)
                drawed = true
            }
            if(!drawed) {
                MainServerService.instance.lastDetectResult.forEach {
                    if (it.oid == targetOid) {
                        targetMid = it.mobileId
                    }
                }
                skippedFrame++
                if(skippedFrame>MAX_SKIP_FRAME) {
                    isTouch = false
                }
            }
        }
    }
}