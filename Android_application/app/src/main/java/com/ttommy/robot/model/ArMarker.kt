package com.ttommy.robot.model

import org.json.JSONArray
import org.json.JSONObject




class ArMarker{
    var id: Int = -1
    var rvec: DoubleArray = DoubleArray(3)
    var tvec: DoubleArray = DoubleArray(3)
    constructor(){

    }
    constructor(id: Int, rvec: DoubleArray, tvec: DoubleArray){
        this.id = id
        this.rvec = rvec
        this.tvec = tvec
    }
    constructor(json: JSONObject){
        //TODO:
    }
    fun toJSON(): JSONObject{
        var result:JSONObject = JSONObject()
        result.put("id", id)
        var tmpArr = JSONArray()
        rvec.forEach { tmpArr.put(it) }
        result.put("rvec", tmpArr)
        tmpArr = JSONArray()
        tvec.forEach { tmpArr.put(it) }
        result.put("tvec", tmpArr)
        return result
    }
}