package com.ttommy.robot.view

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.SurfaceHolder
import android.view.SurfaceView
import com.ttommy.robot.service.RobotService


class LiveControlView(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
): SurfaceView(context, attrs, defStyleAttr), SurfaceHolder.Callback, Runnable{


    private var mHolder: SurfaceHolder? = null
    private var mIsRunning: Boolean = false
    private var mCanvas: Canvas? = null

    var w: Int = 0
    var h: Int = 0



    init{
        mHolder = holder
        mHolder?.addCallback(this)
    }

    override fun surfaceCreated(holder: SurfaceHolder?) {
        mIsRunning = true
        Thread(this).start()
    }
    
    override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {

    }
    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        this.w = w
        this.h = w/4*3
        super.onSizeChanged(this.w, this.h, oldw, oldh)
    }
    override fun surfaceDestroyed(holder: SurfaceHolder?) {
        mIsRunning = false
    }

    override fun run() {
        val start = System.currentTimeMillis()

        while (mIsRunning) {
            draw()
        }
    }
    private fun draw()
    {
        mCanvas = mHolder?.lockCanvas()
        if (mCanvas != null) {
            try {
                if(RobotService.instance.lastFrame !=null) {
                    mCanvas?.drawBitmap(Bitmap.createScaledBitmap(RobotService.instance.lastFrame, w, h, false), 0f, 0f, Paint())
                    val myPaint = Paint()
                    myPaint.color = Color.rgb(255, 0, 0)
                    myPaint.strokeWidth = 3f
                    myPaint.style = Paint.Style.STROKE
                    for(obj in RobotService.instance.lastDetectionRecord){
                        var b = obj.box
                        mCanvas?.drawRect((w*b[1]).toFloat(), (h *b[0]).toFloat(), (w*b[3]).toFloat(), (h *b[2]).toFloat(), myPaint)
                    }
                }
            } catch ( e:Exception) {
                e.printStackTrace()
            } finally {
                mHolder?.unlockCanvasAndPost(mCanvas)
            }
        }
    }
}