package com.ttommy.robot.service

/**
 * Created by TTommy on 3/11/2018.
 */
class ObjectDetectionService{
    private object Holder { val INSTANCE = ObjectDetectionService() }
    var isConnected = false

    companion object {
        val instance: ObjectDetectionService by lazy { Holder.INSTANCE }
    }
}